webpackJsonp([52],{

/***/ 1036:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDeviceModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { GroupModalPage } from "./group-modal/group-modal";

var AddDeviceModalPage = /** @class */ (function () {
    function AddDeviceModalPage(params, viewCtrl, formBuilder, apiCall, toastCtrl, alerCtrl, modalCtrl) {
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.modalCtrl = modalCtrl;
        this.allGroup = [];
        this.devicedetails = {};
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        this.currentYear = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log(this.currentYear);
        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one one later date=> " + __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"));
        // =============== end
        this.addvehicleForm = formBuilder.group({
            device_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_id: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            driver: [''],
            contact_num: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"]],
            sim_number: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            ExipreDate: [this.currentYear, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_type: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            name: [''],
            first_name: [''],
            brand: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            fastag: ['']
        });
        if (params.get("custDet")) {
            this.islogin = params.get("custDet");
            console.log("custDel=> ", this.islogin);
            this.rootLogin = JSON.parse(localStorage.getItem('details')) || {};
        }
        else {
            this.islogin = JSON.parse(localStorage.getItem('details')) || {};
            console.log("islogin devices => " + JSON.stringify(this.islogin));
        }
    }
    AddDeviceModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddDeviceModalPage.prototype.ngOnInit = function () {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
        this.getDrivers();
    };
    AddDeviceModalPage.prototype.getDrivers = function () {
        var _this = this;
        this.apiCall.getDriverList(this.islogin._id)
            .subscribe(function (data) {
            _this.driverList = data;
            console.log("driver list => " + JSON.stringify(_this.driverList));
        }, function (err) {
            console.log(err);
        });
    };
    AddDeviceModalPage.prototype.openAddGroupModal = function () {
        var modal = this.modalCtrl.create('GroupModalPage');
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
        });
        modal.present();
    };
    AddDeviceModalPage.prototype.adddevices = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.addvehicleForm.valid) {
            console.log(this.addvehicleForm.value);
            if (this.params.get("custDet")) {
                if (this.rootLogin.isSuperAdmin == true) {
                    this.devicedetails = {
                        "devicename": this.addvehicleForm.value.device_name,
                        "deviceid": this.addvehicleForm.value.device_id,
                        "driver_name": this.addvehicleForm.value.driver,
                        "contact_number": this.addvehicleForm.value.contact_num,
                        "typdev": "Tracker",
                        "sim_number": this.addvehicleForm.value.sim_number,
                        "user": this.islogin._id,
                        "emailid": this.islogin.email,
                        "iconType": this.TypeOf_Device,
                        // "vehicleGroup": this.groupstaus_id,
                        "device_model": this.modeldata_id,
                        "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                        "supAdmin": this.rootLogin._id
                    };
                }
                else {
                    if (this.rootLogin.isDealer == true) {
                        this.devicedetails = {
                            "devicename": this.addvehicleForm.value.device_name,
                            "deviceid": this.addvehicleForm.value.device_id,
                            "driver_name": this.addvehicleForm.value.driver,
                            "contact_number": this.addvehicleForm.value.contact_num,
                            "typdev": "Tracker",
                            "sim_number": this.addvehicleForm.value.sim_number,
                            "user": this.islogin._id,
                            "emailid": this.islogin.email,
                            "iconType": this.TypeOf_Device,
                            // "vehicleGroup": this.groupstaus_id,
                            "device_model": this.modeldata_id,
                            "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                            "supAdmin": this.rootLogin.supAdmin,
                            "Dealer": this.rootLogin._id
                        };
                    }
                }
                if (this.addvehicleForm.value.fastag != null || this.addvehicleForm.value.fastag != undefined
                    || this.addvehicleForm.value.fastag != '') {
                    this.devicedetails.fastag = this.addvehicleForm.value.fastag;
                }
                if (this.groupstaus != undefined) {
                    this.groupstaus_id = this.groupstaus._id;
                    this.devicedetails.vehicleGroup = this.groupstaus_id;
                }
                if (this.vehType == undefined) {
                    this.devicedetails;
                }
                else {
                    this.devicedetails.vehicleType = this.vehType._id;
                }
            }
            else {
                if (this.islogin.isSuperAdmin == true) {
                    this.devicedetails = {
                        "devicename": this.addvehicleForm.value.device_name,
                        "deviceid": this.addvehicleForm.value.device_id,
                        "driver_name": this.addvehicleForm.value.driver,
                        "contact_number": this.addvehicleForm.value.contact_num,
                        "typdev": "Tracker",
                        "sim_number": this.addvehicleForm.value.sim_number,
                        "user": this.islogin._id,
                        "emailid": this.islogin.email,
                        "iconType": this.TypeOf_Device,
                        // "vehicleGroup": this.groupstaus_id,
                        "device_model": this.modeldata_id,
                        "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                        "supAdmin": this.islogin._id
                    };
                }
                else {
                    if (this.islogin.isDealer == true) {
                        this.devicedetails = {
                            "devicename": this.addvehicleForm.value.device_name,
                            "deviceid": this.addvehicleForm.value.device_id,
                            "driver_name": this.addvehicleForm.value.driver,
                            "contact_number": this.addvehicleForm.value.contact_num,
                            "typdev": "Tracker",
                            "sim_number": this.addvehicleForm.value.sim_number,
                            "user": this.islogin._id,
                            "emailid": this.islogin.email,
                            "iconType": this.TypeOf_Device,
                            // "vehicleGroup": this.groupstaus_id,
                            "device_model": this.modeldata_id,
                            "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                            "supAdmin": this.islogin.supAdmin,
                            "Dealer": this.islogin._id
                        };
                    }
                }
                if (this.addvehicleForm.value.fastag != null || this.addvehicleForm.value.fastag != undefined
                    || this.addvehicleForm.value.fastag != '') {
                    this.devicedetails.fastag = this.addvehicleForm.value.fastag;
                }
                if (this.groupstaus != undefined) {
                    this.groupstaus_id = this.groupstaus._id;
                    this.devicedetails.vehicleGroup = this.groupstaus_id;
                }
                if (this.vehType == undefined) {
                    this.devicedetails;
                }
                else {
                    this.devicedetails.vehicleType = this.vehType._id;
                }
            }
            // if (this.groupstaus == undefined) {
            //     this.groupstaus_id = this.islogin._id;
            // } else {
            //     this.groupstaus_id = this.groupstaus._id;
            // }
            console.log("devicedetails=> " + this.devicedetails);
            this.apiCall.startLoading().present();
            this.apiCall.addDeviceCall(this.devicedetails)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                _this.devicesadd = data;
                console.log("devicesadd=> ", _this.devicesadd);
                var toast = _this.toastCtrl.create({
                    message: 'Vehicle was added successfully',
                    position: 'top',
                    duration: 1500
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.viewCtrl.dismiss(_this.vehType);
                });
                toast.present();
            }, function (err) {
                // this.apiCall.stopLoading();
                // var body = err._body;
                // var msg = JSON.parse(body);
                // let alert = this.alerCtrl.create({
                //     title: 'Oops!',
                //     message: msg.errmsg,
                //     buttons: ['Try Again']
                // });
                // alert.present();
                // console.log("err");
                _this.apiCall.stopLoading();
                // console.log("error occured=> ", err)
                // console.log("error occured=> ", JSON.stringify(err))
                var body = err._body;
                var msg = JSON.parse(body);
                console.log("error occured 1=> ", msg);
                var a = msg.errors;
                console.log("error occured 2=> ", a);
                var b = a.type_of_device;
                console.log("error occured 3=> ", b);
                // var c = JSON.parse(b)
                var alert = _this.alerCtrl.create({
                    title: 'Oops!',
                    message: b.message,
                    buttons: ['Try Again']
                });
                alert.present();
                console.log("err");
            });
        }
    };
    AddDeviceModalPage.prototype.getGroup = function () {
        var _this = this;
        console.log("get group");
        var baseURLp = this.apiCall.mainUrl + 'groups/getGroups_list?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            // this.allGroup = data;
            _this.allGroup = data["group_details"];
            // console.log("all group=> " + this.allGroup[0].name);
            // for (var i = 0; i < this.allGroup.length; i++) {
            //     this.allGroupName = this.allGroup[i].name;
            //     // console.log("allGroupName=> "+this.allGroupName);
            // }
            // console.log("allGroupName=> " + this.allGroupName)
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    AddDeviceModalPage.prototype.getDeviceModel = function () {
        var _this = this;
        console.log("getdevices");
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDeviceModel';
        this.apiCall.getDeviceModelCall(baseURLp)
            .subscribe(function (data) {
            _this.deviceModel = data;
        }, function (err) {
            console.log(err);
        });
    };
    AddDeviceModalPage.prototype.getSelectUser = function () {
        var _this = this;
        console.log("get user");
        var baseURLp = this.apiCall.mainUrl + 'users/getAllUsers?dealer=' + this.islogin._id;
        this.apiCall.getAllUsersCall(baseURLp)
            .subscribe(function (data) {
            _this.selectUser = data;
        }, function (error) {
            console.log(error);
        });
    };
    AddDeviceModalPage.prototype.getVehicleType = function () {
        var _this = this;
        console.log("get getVehicleType");
        var baseURLp = this.apiCall.mainUrl + 'vehicleType/getVehicleTypes?user=' + this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getVehicleTypesCall(baseURLp)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.allVehicle = data;
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    AddDeviceModalPage.prototype.deviceModelata = function (deviceModel) {
        console.log("deviceModel" + deviceModel);
        if (deviceModel != undefined) {
            this.modeldata = deviceModel;
            this.modeldata_id = this.modeldata._id;
        }
        else {
            this.modeldata_id = null;
        }
        // console.log("modal data device_type=> " + this.modeldata.device_type);
    };
    AddDeviceModalPage.prototype.GroupStatusdata = function (status) {
        console.log(status);
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    };
    AddDeviceModalPage.prototype.userselectData = function (userselect) {
        console.log(userselect);
        this.userdata = userselect;
        console.log("userdata=> " + this.userdata.first_name);
    };
    AddDeviceModalPage.prototype.vehicleTypeselectData = function (vehicletype) {
        console.log(vehicletype);
        this.vehType = vehicletype;
        console.log("vehType=> " + this.vehType._id);
    };
    AddDeviceModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"D:\Pro\tcs2_gps\src\pages\customers\modals\add-device-modal.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{\'Add Vehicle\' | translate}}</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="addvehicleForm">\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Device ID(IMEI)*\' | translate}}</ion-label>\n\n            <ion-input formControlName="device_id" type="text" style="margin-left: -2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addvehicleForm.controls.device_id.valid && (addvehicleForm.controls.device_id.dirty || submitAttempt)">\n\n            <p>{{\'device id required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Registration Number*\' | translate}}</ion-label>\n\n            <ion-input formControlName="device_name" type="text" style="margin-left: -2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addvehicleForm.controls.device_name.valid && (addvehicleForm.controls.device_name.dirty || submitAttempt)">\n\n            <p>{{\'device name required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Sim Number*\'| translate}}</ion-label>\n\n            <ion-input formControlName="sim_number" type="number" maxlength="13" minlength="10"\n\n                style="margin-left: -2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addvehicleForm.controls.sim_number.valid && (addvehicleForm.controls.sim_number.dirty || submitAttempt)">\n\n            <p>{{\'sim number is required & sould be 10 or 13 digits!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>{{\'Driver Name\' | translate}}</ion-label>\n\n            <ion-select formControlName="driver" style="min-width:50%;">\n\n                <ion-option *ngFor="let dr of driverList" [value]="dr.name" style="margin-left: -2px;">{{dr.name}}\n\n                </ion-option>\n\n            </ion-select>\n\n\n\n        </ion-item>\n\n\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Driver Contact Num.\' | translate}}</ion-label>\n\n            <ion-input formControlName="contact_num" type="number" maxlength="10" minlength="10"\n\n                style="margin-left: -2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addvehicleForm.controls.contact_num.valid && (addvehicleForm.controls.contact_num.dirty || submitAttempt)">\n\n            <p>{{\'contact number should be 10 digits!\'| translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Expire On*\' | translate}}</ion-label>\n\n            <ion-input type="date" formControlName="ExipreDate" style="margin-left: -2px;" min="{{minDate}}">\n\n            </ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addvehicleForm.controls.ExipreDate.valid && (addvehicleForm.controls.ExipreDate.dirty || submitAttempt)">\n\n            <p>{{\'date of expiry required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>{{\'Device Model\'| translate}}*</ion-label>\n\n            <ion-select formControlName="device_type" style="min-width:50%;">\n\n                <ion-option *ngFor="let name of deviceModel" [value]="name.device_type"\n\n                    (ionSelect)="deviceModelata(name)">{{name.device_type}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addvehicleForm.controls.device_type.valid && (addvehicleForm.controls.device_type.dirty || submitAttempt)">\n\n            <p>{{\'device model required!\'| translate}}</p>\n\n        </ion-item>\n\n\n\n\n\n        <ion-item *ngIf="allGroup.length > 0">\n\n            <ion-label>{{\'Group\' | translate}}</ion-label>\n\n            <ion-select formControlName="name" style="min-width:50%;">\n\n                <ion-option *ngFor="let group of allGroup" [value]="group.name" (ionSelect)="GroupStatusdata(group)">\n\n                    {{group.name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>{{\'Select User\'| translate}}</ion-label>\n\n            <ion-select formControlName="first_name" style="min-width:50%;">\n\n                <ion-option *ngFor="let user of selectUser" [value]="user.first_name"\n\n                    (ionSelect)="userselectData(user)">{{user.first_name | titlecase}} {{user.last_name | titlecase}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>{{\'Vehicle Type\' | translate}}*</ion-label>\n\n            <ion-select formControlName="brand" style="min-width: 50%;">\n\n                <ion-option *ngFor="let veh of allVehicle" [value]="veh.brand" (ionSelect)="vehicleTypeselectData(veh)">\n\n                    {{veh.brand}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addvehicleForm.controls.brand.valid && (addvehicleForm.controls.brand.dirty || submitAttempt)">\n\n            <p>{{\'vehicle type is required required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <!-- <ion-item>\n\n            <ion-label>{{\'Fast tag Id\' | translate}}</ion-label>\n\n            <ion-input formControlName="fastag" type="text" style="margin-left: -2px;"></ion-input>\n\n        </ion-item> -->\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Fast tag Id\' | translate}}</ion-label>\n\n            <ion-input formControlName="fastag" type="text" style="margin-left: -2px;"></ion-input>\n\n        </ion-item>\n\n        <!-- <ion-item class="logitem1" *ngIf="!addvehicleForm.controls.brand.valid && (addvehicleForm.controls.brand.dirty || submitAttempt)">\n\n            <p>{{\'vehicle type is required required!\' | translate}}</p>\n\n        </ion-item> -->\n\n    </form>\n\n</ion-content>\n\n\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row no-padding>\n\n            <ion-col width-50 style="border-right: 1px solid white; text-align: center; padding: 0px !important">\n\n                <button ion-button style="padding: 0px;" clear color="light"\n\n                    (click)="openAddGroupModal()">{{\'ADD NEW GROUP\' | translate}}</button>\n\n            </ion-col>\n\n            <ion-col width-50 style="text-align: center; padding: 0px !important">\n\n                <button ion-button style="padding: 0px;" clear color="light"\n\n                    (click)="adddevices()">{{\'ADD VEHICLE\' | translate}}</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\Pro\tcs2_gps\src\pages\customers\modals\add-device-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], AddDeviceModalPage);
    return AddDeviceModalPage;
}());

//# sourceMappingURL=add-device-modal.js.map

/***/ }),

/***/ 583:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDeviceModalPageModule", function() { return AddDeviceModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_device_modal__ = __webpack_require__(1036);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AddDeviceModalPageModule = /** @class */ (function () {
    function AddDeviceModalPageModule() {
    }
    AddDeviceModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_device_modal__["a" /* AddDeviceModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_device_modal__["a" /* AddDeviceModalPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ]
        })
    ], AddDeviceModalPageModule);
    return AddDeviceModalPageModule;
}());

//# sourceMappingURL=add-device-modal.module.js.map

/***/ })

});
//# sourceMappingURL=52.js.map