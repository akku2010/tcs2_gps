webpackJsonp([30],{

/***/ 1051:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(440);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Subscription__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__live_single_device_live_single_device__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_geocoder_geocoder__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var LivePage = /** @class */ (function () {
    function LivePage(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, socialSharing, alertCtrl, modalCtrl, plt, viewCtrl, translate, toastCtrl, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.plt = plt;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        // shouldBounce = true;
        // dockedHeight = 52;
        // distanceTop = 150;
        // drawerState = DrawerState.Docked;
        // states = DrawerState;
        // minimumHeight = 52;
        this.shouldBounce = true;
        this.dockedHeight = 80;
        this.distanceTop = 200;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 50;
        this.transition = '0.85s ease-in-out';
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.allData = {};
        this.isEnabled = false;
        this.showMenuBtn = false;
        this.mapHideTraffic = false;
        this.mapData = [];
        this.geodata = [];
        this.geoShape = [];
        this.locations = [];
        this.shwBckBtn = false;
        this.deviceDeatils = {};
        this.showaddpoibtn = false;
        this.zoomLevel = 18;
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.carIcon,
            "truck": this.carIcon,
            "bus": this.carIcon,
            "user": this.carIcon,
            "jcb": this.carIcon,
            "tractor": this.carIcon
        };
        this.resumeListener = new __WEBPACK_IMPORTED_MODULE_9_rxjs_Subscription__["Subscription"]();
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == this.translate.instant('Hybrid')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == this.translate.instant('Normal')) {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == this.translate.instant('Terrain')) {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == this.translate.instant('Satellite')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        this.menuActive = false;
    }
    LivePage.prototype.goBack = function () {
        this.navCtrl.pop({ animate: true, direction: 'forward' });
    };
    LivePage.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    LivePage.prototype.refreshMe = function () {
        this.ngOnDestroy();
        this.ngOnInit();
    };
    LivePage.prototype.refreshMe1 = function () {
        var that = this;
        this.ngOnDestroy();
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.apiCall.mainUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        var paramData = that.data;
        this.socketInit(paramData);
    };
    LivePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.plt.is('ios')) {
            this.shwBckBtn = true;
            this.viewCtrl.showBackButton(false);
        }
        this.plt.ready().then(function () {
            _this.resumeListener = _this.plt.resume.subscribe(function () {
                var today, Christmas;
                today = new Date();
                Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
                var diffMs = (today - Christmas); // milliseconds between now & Christmas
                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                if (diffMins >= 5) {
                    localStorage.removeItem("backgroundModeTime");
                    _this.alertCtrl.create({
                        message: _this.translate.instant("Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?"),
                        buttons: [
                            {
                                text: _this.translate.instant('YES PROCEED'),
                                handler: function () {
                                    // this.getdevices();
                                }
                            },
                            {
                                text: _this.translate.instant('Back'),
                                handler: function () {
                                    _this.navCtrl.setRoot('DashboardPage');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    };
    LivePage.prototype.ionViewWillLeave = function () {
        var _this = this;
        this.plt.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
    };
    LivePage.prototype.newMap = function () {
        var mapOptions = {
            controls: {
                zoom: false,
                myLocation: true,
            },
            mapTypeControlOptions: {
                mapTypeIds: [__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].ROADMAP, 'map_style']
            },
            gestures: {
                rotate: false,
                tilt: false
            },
            mapType: this.mapKey
        };
        var map = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
        if (this.plt.is('android')) {
            map.animateCamera({
                target: { lat: 20.5937, lng: 78.9629 },
                duration: 2000,
            });
            map.setPadding(20, 20, 20, 20);
        }
        return map;
    };
    LivePage.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE') {
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].SATELLITE);
        }
        else {
            if (maptype == 'TERRAIN') {
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
            }
            else {
                if (maptype == 'NORMAL') {
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                }
            }
        }
    };
    LivePage.prototype.settings = function () {
        var _this = this;
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: that.deviceDeatils
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            for (var i = 0; i < that.socketChnl.length; i++)
                that._io.removeAllListeners(that.socketChnl[i]);
            var paramData = _this.navParams.get("device");
            _this.temp(paramData);
        });
    };
    LivePage.prototype.addPOI = function () {
        var _this = this;
        var that = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__live_single_device_live_single_device__["b" /* PoiPage */], {
            param1: that.allData[that.impkey]
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            var that = _this;
            that.showaddpoibtn = false;
        });
        modal.present();
    };
    LivePage.prototype.onSelectMapOption = function (type) {
        var that = this;
        if (type == 'locateme') {
            that.allData.map.setCameraTarget(that.latlngCenter);
        }
        else {
            if (type == 'mapHideTraffic') {
                that.mapHideTraffic = !that.mapHideTraffic;
                if (that.mapHideTraffic) {
                    that.allData.map.setTrafficEnabled(true);
                }
                else {
                    that.allData.map.setTrafficEnabled(false);
                }
            }
            else {
                if (type == 'showGeofence') {
                    console.log("Show Geofence");
                    if (that.generalPolygon != undefined) {
                        that.generalPolygon.remove();
                        that.generalPolygon = undefined;
                    }
                    else {
                        that.callGeofence();
                    }
                }
            }
        }
    };
    LivePage.prototype.callGeofence = function () {
        var _this = this;
        this.geoShape = [];
        this.apiCall.startLoading().present();
        this.apiCall.getGeofenceCall(this.userdetails._id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("geofence data=> " + data.length);
            if (data.length > 0) {
                _this.geoShape = data.map(function (d) {
                    return d.geofence.coordinates[0];
                });
                for (var g = 0; g < _this.geoShape.length; g++) {
                    for (var v = 0; v < _this.geoShape[g].length; v++) {
                        _this.geoShape[g][v] = _this.geoShape[g][v].reverse();
                    }
                }
                for (var t = 0; t < _this.geoShape.length; t++) {
                    _this.drawPolygon(_this.geoShape[t]);
                }
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    message: 'No gofence found..!!',
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.drawPolygon = function (polyData) {
        var _this = this;
        var that = this;
        that.geodata = [];
        that.geodata = polyData.map(function (d) {
            return { lat: d[0], lng: d[1] };
        });
        console.log("geodata=> ", that.geodata);
        // let bounds = new LatLngBounds(that.geodata);
        // that.allData.map.moveCamera({
        // target: bounds
        // });
        var GORYOKAKU_POINTS = that.geodata;
        console.log("GORYOKAKU_POINTS=> ", GORYOKAKU_POINTS);
        that.allData.map.addPolygon({
            'points': GORYOKAKU_POINTS,
            'strokeColor': '#AA00FF',
            'fillColor': '#00FFAA',
            'strokeWidth': 2
        }).then(function (polygon) {
            // polygon.on(GoogleMapsEvent.POLYGON_CLICK).subscribe((param) => {
            //   console.log("polygon data=> " + param)
            //   console.log("polygon data=> " + param[1])
            // })
            _this.generalPolygon = polygon;
        });
    };
    LivePage.prototype.ngOnInit = function () {
        if (localStorage.getItem("SCREEN") != null) {
            if (localStorage.getItem("SCREEN") === 'live') {
                this.showMenuBtn = true;
            }
        }
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.apiCall.mainUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        // this.drawerHidden = true;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Bottom;
        this.onClickShow = false;
        this.showBtn = false;
        this.SelectVehicle = "Select Vehicle";
        this.selectedVehicle = undefined;
        this.userDevices();
    };
    LivePage.prototype.ngOnDestroy = function () {
        var _this = this;
        for (var i = 0; i < this.socketChnl.length; i++)
            this._io.removeAllListeners(this.socketChnl[i]);
        this._io.on('disconnect', function () {
            _this._io.open();
        });
    };
    LivePage.prototype.shareLive = function () {
        var _this = this;
        var data = {
            id: this.liveDataShare._id,
            imei: this.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: 60 // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.getPOIs = function () {
        var _this = this;
        this.allData._poiData = [];
        var _burl = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_burl)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.allData._poiData = data;
            _this.plotPOI();
        });
    };
    LivePage.prototype.plotPOI = function () {
        var toast = this.toastCtrl.create({
            message: "Plotting POI's please wait...",
            position: 'middle'
        });
        for (var v = 0; v < this.allData._poiData.length; v++) {
            toast.present();
            if (this.allData._poiData[v].poi.location.coordinates !== undefined) {
                this.allData.map.addMarker({
                    title: this.allData._poiData[v].poi.poiname,
                    position: {
                        lat: this.allData._poiData[v].poi.location.coordinates[1],
                        lng: this.allData._poiData[v].poi.location.coordinates[0]
                    },
                    icon: './assets/imgs/poiFlag.png',
                    animation: __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["c" /* GoogleMapsAnimation */].BOUNCE
                });
            }
        }
        toast.dismiss();
    };
    LivePage.prototype.liveShare = function () {
        var that = this;
        var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
        that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
    };
    LivePage.prototype.zoomin = function () {
        var that = this;
        that.allData.map.moveCameraZoomIn();
    };
    LivePage.prototype.zoomout = function () {
        var that = this;
        that.allData.map.animateCameraZoomOut();
    };
    LivePage.prototype.userDevices = function () {
        var baseURLp;
        var that = this;
        if (that.allData.map != undefined) {
            that.allData.map.remove();
            that.allData.map = that.newMap();
        }
        else {
            that.allData.map = that.newMap();
        }
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;
        if (this.userdetails.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.userdetails._id;
            this.isSuperAdmin = true;
        }
        else {
            if (this.userdetails.isDealer == true) {
                baseURLp += '&dealer=' + this.userdetails._id;
                this.isDealer = true;
            }
        }
        that.apiCall.startLoading().present();
        that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (resp) {
            that.apiCall.stopLoading();
            that.portstemp = resp.devices;
            console.log("list of vehicles :", that.portstemp);
            that.mapData = [];
            that.mapData = that.portstemp.map(function (d) {
                if (d.last_location !== undefined) {
                    return { lat: d.last_location['lat'], lng: d.last_location['long'] };
                }
            });
            var dummyData;
            dummyData = that.portstemp.map(function (d) {
                if (d.last_location === undefined)
                    return;
                else {
                    return {
                        "position": {
                            "lat": d.last_location['lat'],
                            "lng": d.last_location['long']
                        },
                        "name": d.Device_Name,
                        "icon": that.getIconUrl(d)
                    };
                }
            });
            dummyData = dummyData.filter(function (element) {
                return element !== undefined;
            });
            console.log("dummy data: ", dummyData);
            // console.log("dummy data: ", this.dummyData());
            var bounds = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
            that.allData.map.moveCamera({
                target: bounds,
                zoom: 10
            });
            // this.innerFunc(that.portstemp);
            // this.addCluster(this.dummyData());
            if (that.isSuperAdmin || that.isDealer) {
                for (var t = 0; t < dummyData.length; t++) {
                    console.log("check position: ", dummyData[t].position);
                }
                // that.addCluster(this.dummyData());
                that.addCluster(dummyData);
            }
            else {
                for (var i = 0; i < resp.devices.length; i++) {
                    if (resp.devices[i].status != "Expired") {
                        // if (that.isSuperAdmin || that.isDealer) {
                        //   if (resp.devices[i].last_location !== undefined) {
                        //     // that.allData.map.addMarker({
                        //     //   title: resp.devices[i].Device_Name,
                        //     //   position: {
                        //     //     lat: resp.devices[i].last_location.lat,
                        //     //     lng: resp.devices[i].last_location.long
                        //     //   },
                        //     //   icon: that.getIconUrl(resp.devices[i])
                        //     // })
                        //   }
                        // } else {
                        that.socketInit(resp.devices[i]);
                        // }
                    }
                }
            }
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.dummyData = function () {
        return [
            {
                "position": {
                    "lat": 21.382314,
                    "lng": -157.933097
                },
                "name": "Starbucks - HI - Aiea  03641",
                "address": "Aiea Shopping Center_99-115\nAiea Heights Drive #125_Aiea, Hawaii 96701",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.3871,
                    "lng": -157.9482
                },
                "name": "Starbucks - HI - Aiea  03642",
                "address": "Pearlridge Center_98-125\nKaonohi Street_Aiea, Hawaii 96701",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.363402,
                    "lng": -157.928275
                },
                "name": "Starbucks - HI - Aiea  03643",
                "address": "Stadium Marketplace_4561\nSalt Lake Boulevard_Aiea, Hawaii 96818",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.3884,
                    "lng": -157.9431
                },
                "name": "Starbucks - HI - Aiea  03644",
                "address": "Pearlridge Mall_98-1005\nMoanalua Road_Aiea, Hawaii 96701",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.6507,
                    "lng": -158.0652
                },
                "name": "Starbucks - HI - Haleiwa  03645",
                "address": "Pupukea_59-720 Kamehameha Highway\nHaleiwa, Hawaii 96712",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 19.699335,
                    "lng": -155.06566
                },
                "name": "Starbucks - HI - Hilo  03646",
                "address": "Border Waiakea Center_315-325\nMakaala Street_Hilo, Hawaii 96720",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 19.698399,
                    "lng": -155.064864
                },
                "name": "Starbucks - HI - Hilo  03647",
                "address": "Prince Kuhio Plaza_111\nEast Puainako Street_Hilo, Hawaii 96720",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 19.722763,
                    "lng": -155.085598
                },
                "name": "Starbucks - HI - Hilo [D]  03648",
                "address": "Hilo_438 Kilauea Ave_Hilo, Hawaii 96720",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.338183,
                    "lng": -157.917579
                },
                "name": "Starbucks - HI - Honolulu  03649",
                "address": "Airport Trade Center_550\nPaiea Street_Honolulu, Hawaii 96819",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.3074,
                    "lng": -157.865199
                },
                "name": "Starbucks - HI - Honolulu  03650",
                "address": "Aloha Tower_1 Aloha Tower Drive\nHonolulu, Hawaii 96813",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.30846253,
                    "lng": -157.8614898
                },
                "name": "Starbucks - HI - Honolulu  03651",
                "address": "Bishop_1000 Bishop Street #104\nHonolulu, Hawaii 96813",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.311363,
                    "lng": -157.863751
                },
                "name": "Starbucks - HI - Honolulu  03652",
                "address": "Central Pacific Bank_220 South King Street\nHonolulu, Hawaii 96813",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.28507546,
                    "lng": -157.838214
                },
                "name": "Starbucks - HI - Honolulu  03653",
                "address": "Discovery Bay_1778 Ala Moana Boulevard\nHonolulu, Hawaii 96815",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.342733,
                    "lng": -158.027408
                },
                "name": "Starbucks - HI - Honolulu  03654",
                "address": "Ewa Beach_91-1401 Fort Weaver Road\nHonolulu, Hawaii 96706",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.28005068,
                    "lng": -157.8285093
                },
                "name": "Starbucks - HI - Honolulu  03655",
                "address": "Duty Free Shopper_330 Royal Hawaiian Avenue\nHonolulu, Hawaii 96815",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.3115,
                    "lng": -157.8649
                },
                "name": "Starbucks - HI - Honolulu  03656",
                "address": "Financial Plaza_130 Merchant Street #111\nHonolulu, Hawaii 96813",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.282048,
                    "lng": -157.713041
                },
                "name": "Starbucks - HI - Honolulu  03657",
                "address": "Hawaii Kai Town Center_6700\nKalanianaole Highway_Honolulu, Hawaii 96825",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.291345,
                    "lng": -157.848791
                },
                "name": "Starbucks - HI - Honolulu  03658",
                "address": "Hokua_1288 Ala Moana Blvd\nHonolulu, Hawaii 96814",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.335164,
                    "lng": -157.868742
                },
                "name": "Starbucks - HI - Honolulu  03659",
                "address": "Kamehameha Shopping Center_1620 North School Street\nHonolulu, Hawaii 96817",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.27852422,
                    "lng": -157.7875773
                },
                "name": "Starbucks - HI - Honolulu  03660",
                "address": "Kahala Mall_4211 Waialae Avenue\nHonolulu, Hawaii 96816",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.294372,
                    "lng": -157.841963
                },
                "name": "Starbucks - HI - Honolulu  03661",
                "address": "Keeaumoku_678 Keeamoku Street #106\nHonolulu, Hawaii 96814",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.2819,
                    "lng": -157.8163
                },
                "name": "Starbucks - HI - Honolulu  03662",
                "address": "Kapahulu Avenue_625 Kapahulu Avenue\nHonolulu, Hawaii 96815",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.27608195,
                    "lng": -157.7049835
                },
                "name": "Starbucks - HI - Honolulu  03663",
                "address": "Koko Marina_7192 Kalanianaole Highway\nHonolulu, Hawaii 96825",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.3129,
                    "lng": -157.8129
                },
                "name": "Starbucks - HI - Honolulu  03664",
                "address": "Manoa Valley_2902 East Manoa Road\nHonolulu, Hawaii 96822",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.293082,
                    "lng": -157.847092
                },
                "name": "Starbucks - HI - Honolulu  03665",
                "address": "Macys Ala Moana_1450 Ala Moan Boulevard\nHonolulu, Hawaii 96814",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.341957,
                    "lng": -157.929089
                },
                "name": "Starbucks - HI - Honolulu  03666",
                "address": "Moanalua Shopping Center_930 Valkenburgh Street\nHonolulu, Hawaii 96818",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.279503,
                    "lng": -157.833101
                },
                "name": "Starbucks - HI - Honolulu  03667",
                "address": "Outrigger Reef_2169 Kalia Road #102\nHonolulu, Hawaii 96815",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.282251,
                    "lng": -157.828172
                },
                "name": "Starbucks - HI - Honolulu  03668",
                "address": "Ohana Waikiki West_2330 Kuhio Avenue\nHonolulu, Hawaii 96815",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.323602,
                    "lng": -157.890904
                },
                "name": "Starbucks - HI - Honolulu  03669",
                "address": "Sand Island_120 Sand Island Access Road #4\nHonolulu, Hawaii 96819",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.270016,
                    "lng": -157.82381
                },
                "name": "Starbucks - HI - Honolulu  03670",
                "address": "Park Shore Hotel_2856 Kalakaua Avenue\nHonolulu, Hawaii 96815",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.289497,
                    "lng": -157.842832
                },
                "name": "Starbucks - HI - Honolulu  03671",
                "address": "Sears Ala Moana Center_1450 Ala Moana Blvd.\nHonolulu, Hawaii 96814",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.28201,
                    "lng": -157.831087
                },
                "name": "Starbucks - HI - Honolulu  03672",
                "address": "Waikiki Shopping Plaza_2270 Kalakaua Avenue #1800\nHonolulu, Hawaii 96815",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.2833,
                    "lng": -157.8298
                },
                "name": "Starbucks - HI - Honolulu  03673",
                "address": "Waikiki Trade Center_2255 Kuhio Avenue S-1\nHonolulu, Hawaii 96815",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.297,
                    "lng": -157.8555
                },
                "name": "Starbucks - HI - Honolulu  03674",
                "address": "Ward Entertainment Center_310 Kamakee Street #6\nHonolulu, Hawaii 96814",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.406095,
                    "lng": -157.800761
                },
                "name": "Starbucks - HI - Honolulu  03675",
                "address": "Windward City Shopping Center_45-480 Kaneohe Bay Drive\nHonolulu, Hawaii 96744",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.2829,
                    "lng": -157.8318
                },
                "name": "Starbucks - HI - Honolulu  03676",
                "address": "Waikiki Walk_2222 Kalakaua Avenue\nHonolulu, Hawaii 96815",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.293045,
                    "lng": -157.852223
                },
                "name": "Starbucks - HI - Honolulu  03677",
                "address": "Ward Gateway_1142 Auahi Street\nHonolulu, Hawaii 96814",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.3067,
                    "lng": -157.858444
                },
                "name": "Starbucks - HI - Honolulu [A]  03678",
                "address": "HNL Honolulu Airport_300 Rogers Blvd\nHonolulu, Hawaii 96820",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 20.891963,
                    "lng": -156.477614
                },
                "name": "Starbucks - HI - Kahului  03679",
                "address": "Queen Kaahumanu Center_275 West Kaahuman Avenue #1200 F5\nKahului, Hawaii 96732",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 20.8843,
                    "lng": -156.4583
                },
                "name": "Starbucks - HI - Kahului  03680",
                "address": "Maui Marketplace_270 Dairy Road\nKahului, Hawaii 96732",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.39356972,
                    "lng": -157.7403334
                },
                "name": "Starbucks - HI - Kailua  03681",
                "address": "Kailua Village_539 Kailua Road\nKailua, Hawaii 96734",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 19.6512,
                    "lng": -155.9944
                },
                "name": "Starbucks - HI - Kailua-Kona  03682",
                "address": "Kona Coast Shopping Center_74-5588 Palani Road\nKailua-Kona, Hawaii 96740",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 19.8434,
                    "lng": -155.7652
                },
                "name": "Starbucks - HI - Kamuela  03683",
                "address": "Parker Ranch Center_67-1185 Mamalahoa Highway D108\nKamuela, Hawaii 96743",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.34306,
                    "lng": -158.078906
                },
                "name": "Starbucks - HI - Kapolei  03684",
                "address": "Halekuai Center_563 Farrington Highway #101\nKapolei, Hawaii 96707",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.336,
                    "lng": -158.0802
                },
                "name": "Starbucks - HI - Kapolei [D]  03685",
                "address": "Kapolei Parkway_338 Kamokila Boulevard #108\nKapolei, Hawaii 96797",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 20.740343,
                    "lng": -156.456218
                },
                "name": "Starbucks - HI - Kihei  03686",
                "address": "Kukui Mall_1819 South Kihei Road\nKihei, Hawaii 96738",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 20.7575,
                    "lng": -156.4559
                },
                "name": "Starbucks - HI - Kihei  03687",
                "address": "Piilani Village Shopping Center_247 Piikea Avenue #106\nKihei, Hawaii 96753",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 19.9338,
                    "lng": -155.8422
                },
                "name": "Starbucks - HI - Kohala Coast  03688",
                "address": "Mauna Lani_68-1330 Mauna Lani Drive H-101b\nKohala Coast, Hawaii 96743",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 20.886244,
                    "lng": -156.684697
                },
                "name": "Starbucks - HI - Lahaina  03689",
                "address": "Lahaina Cannery Mall_1221 Honoapiilani Highway\nLahaina, Hawaii 96761",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 20.8804,
                    "lng": -156.6816
                },
                "name": "Starbucks - HI - Lahaina  03690",
                "address": "Lahaina_845 Wainee Street\nLahaina, Hawaii 96761",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.970661,
                    "lng": -159.396274
                },
                "name": "Starbucks - HI - Lihue  03691",
                "address": "Kukui Grove_3-2600 Kaumualii Highway #A8\nLihue, Hawaii 96766",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 20.8818,
                    "lng": -156.4633
                },
                "name": "Starbucks - HI - Maui [A]  03692",
                "address": "OGG Kahului Main Concourse_New Terminal Bldg @ Bldg 340\nMaui, Hawaii 96732",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.4585,
                    "lng": -158.0178
                },
                "name": "Starbucks - HI - Mililani  03693",
                "address": "Mililani Shopping Center_95-221 Kipapa Drive\nMililani, Hawaii 96789",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.474341,
                    "lng": -158.001864
                },
                "name": "Starbucks - HI - Mililani  03694",
                "address": "Mililani Town Center_95-1249 Meheula Parkway\nMililani, Hawaii 96789",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 20.838965,
                    "lng": -156.34192
                },
                "name": "Starbucks - HI - Pukalani  03695",
                "address": "Pukalani Foodland_55 Pukalani Street\nPukalani, Hawaii 96768",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.378675,
                    "lng": -157.728499
                },
                "name": "Starbucks - HI - Waipahu  03696",
                "address": "Enchanted Lakes_1020 Keolu Drive\nWaipahu, Hawaii 96734",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.39662113,
                    "lng": -158.0317397
                },
                "name": "Starbucks - HI - Waipahu  03697",
                "address": "Kunia Shopping Center_94-673 Kupuohi Street\nWaipahu, Hawaii 96797",
                "icon": "assets/markercluster/marker.png"
            },
            {
                "position": {
                    "lat": 21.403688,
                    "lng": -158.006128
                },
                "name": "Starbucks - HI - Waipahu  03698",
                "address": "Waikele_94-799 Lumiaina Street\nWaipahu, Hawaii 96797",
                "icon": "assets/markercluster/marker.png"
            }
        ];
    };
    LivePage.prototype.getIconUrl = function (data) {
        var that = this;
        var iconUrl;
        if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
            if (that.plt.is('ios')) {
                iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            }
            else if (that.plt.is('android')) {
                iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
        }
        else {
            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
                if (that.plt.is('ios')) {
                    iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
            }
            else {
                if (that.plt.is('ios')) {
                    iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                }
            }
        }
        return iconUrl;
    };
    LivePage.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LivePage.prototype.socketInit = function (pdata, center) {
        if (center === void 0) { center = false; }
        var that = this;
        that._io.emit('acc', pdata.Device_ID);
        that.socketChnl.push(pdata.Device_ID + 'acc');
        that._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    if (data._id != undefined && data.last_location != undefined) {
                        var key = data._id;
                        that.impkey = data._id;
                        that.deviceDeatils = data;
                        var ic_1 = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](that.icons[data.iconType]);
                        if (!ic_1) {
                            return;
                        }
                        ic_1.path = null;
                        if (data.status.toLowerCase() === 'running' && data.last_speed > 0) {
                            if (that.plt.is('ios')) {
                                ic_1.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                            }
                            else if (that.plt.is('android')) {
                                ic_1.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
                            }
                        }
                        else {
                            if (data.status.toLowerCase() === 'running' && data.last_speed === 0) {
                                if (that.plt.is('ios')) {
                                    ic_1.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                                }
                                else if (that.plt.is('android')) {
                                    ic_1.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                                }
                            }
                            else {
                                if (data.status.toLowerCase() === 'idling' && data.last_speed === 0) {
                                    if (that.plt.is('ios')) {
                                        ic_1.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                                    }
                                    else if (that.plt.is('android')) {
                                        ic_1.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                                    }
                                }
                                else {
                                    if (data.status.toLowerCase() === 'idling' && data.last_speed > 0) {
                                        if (that.plt.is('ios')) {
                                            ic_1.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                                        }
                                        else if (that.plt.is('android')) {
                                            ic_1.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
                                        }
                                    }
                                    else {
                                        if (that.plt.is('ios')) {
                                            ic_1.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                                        }
                                        else if (that.plt.is('android')) {
                                            ic_1.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                                        }
                                    }
                                }
                            }
                        }
                        console.log("ic url=> " + ic_1.url);
                        that.vehicle_speed = data.last_speed;
                        that.todays_odo = data.today_odo;
                        that.total_odo = data.total_odo;
                        // that.fuel = data.currentFuel;
                        if (that.userdetails.fuel_unit == 'LITRE') {
                            that.fuel = data.currentFuel;
                        }
                        else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
                            that.fuel = data.fuel_percent;
                        }
                        else {
                            that.fuel = data.currentFuel;
                        }
                        that.last_ping_on = data.last_ping_on;
                        if (data.lastStoppedAt != null) {
                            var fd = new Date(data.lastStoppedAt).getTime();
                            var td = new Date().getTime();
                            var time_difference = td - fd;
                            var total_min = time_difference / 60000;
                            var hours = total_min / 60;
                            var rhours = Math.floor(hours);
                            var minutes = (hours - rhours) * 60;
                            var rminutes = Math.round(minutes);
                            that.lastStoppedAt = rhours + ':' + rminutes;
                        }
                        else {
                            that.lastStoppedAt = '00' + ':' + '00';
                        }
                        that.distFromLastStop = data.distFromLastStop;
                        if (!isNaN(data.timeAtLastStop)) {
                            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
                        }
                        else {
                            that.timeAtLastStop = '00:00:00';
                        }
                        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
                        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
                        that.last_ACC = data.last_ACC;
                        that.acModel = data.ac;
                        that.currentFuel = data.currentFuel;
                        that.power = data.power;
                        that.gpsTracking = data.gpsTracking;
                        that.recenterMeLat = data.last_location.lat;
                        that.recenterMeLng = data.last_location.long;
                        if (that.allData[key]) {
                            that.socketSwitch[key] = data;
                            if (that.allData[key].mark != undefined) {
                                that.allData[key].mark.setIcon(ic_1);
                                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                                var temp = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](that.allData[key].poly[1]);
                                that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](temp);
                                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                that.getAddress(data.last_location);
                                that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                            }
                            else {
                                return;
                            }
                        }
                        else {
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData[key].poly = [];
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {
                                that.allData.map.addMarker({
                                    title: data.Device_Name,
                                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                    icon: ic_1,
                                }).then(function (marker) {
                                    that.allData[key].mark = marker;
                                    // if (that.selectedVehicle == undefined) {
                                    //   marker.showInfoWindow();
                                    // }
                                    // if (that.selectedVehicle != undefined) {
                                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                        .subscribe(function (e) {
                                        if (that.selectedVehicle == undefined) {
                                            that.getAddressTitle(marker);
                                        }
                                        else {
                                            that.liveVehicleName = data.Device_Name;
                                            // that.drawerHidden = false;
                                            that.showaddpoibtn = true;
                                            that.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                                            that.onClickShow = true;
                                        }
                                    });
                                    that.getAddress(data.last_location);
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                });
                            }
                        }
                    }
                })(d4);
        });
    };
    LivePage.prototype.getAddress = function (coordinates) {
        var that = this;
        if (!coordinates) {
            that.address = 'N/A';
            return;
        }
        this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            that.saveAddressToServer(str, coordinates.lat, coordinates.long);
            that.address = str;
        });
    };
    LivePage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    LivePage.prototype.showNearby = function (key) {
        var _this = this;
        // for police stations
        var url, icurl;
        if (key === 'police') {
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + this.recenterMeLat + "," + this.recenterMeLng + "&radius=1000&types=police&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
            icurl = './assets/imgs/police-station.png';
        }
        else if (key === 'petrol') {
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + this.recenterMeLat + "," + this.recenterMeLng + "&radius=1000&types=gas_station&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
            icurl = './assets/imgs/gas-pump.png';
        }
        console.log("google maps activity: ", url);
        this.apiCall.getSOSReportAPI(url).subscribe(function (resp) {
            console.log(resp.status);
            if (resp.status === 'OK') {
                console.log(resp.results);
                console.log(resp);
                var mapData = resp.results.map(function (d) {
                    return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
                });
                var bounds = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["g" /* LatLngBounds */](mapData);
                for (var t = 0; t < resp.results.length; t++) {
                    _this.allData.map.addMarker({
                        title: resp.results[t].name,
                        position: {
                            lat: resp.results[t].geometry.location.lat,
                            lng: resp.results[t].geometry.location.lng
                        },
                        icon: icurl
                    }).then(function (mark) {
                    });
                }
                _this.allData.map.moveCamera({
                    target: bounds
                });
                _this.allData.map.setPadding(20, 20, 20, 20);
                _this.zoomLevel = 10;
            }
            else if (resp.status === 'ZERO_RESULTS') {
                if (key === 'police') {
                    _this.showToastMsg('No nearby police stations found.');
                }
                else if (key === 'petrol') {
                    _this.showToastMsg('No nearby petrol pump found.');
                }
            }
            else if (resp.status === 'OVER_QUERY_LIMIT') {
                _this.showToastMsg('Query limit exeed. Please try after some time.');
            }
            else if (resp.status === 'REQUEST_DENIED') {
                _this.showToastMsg('Please check your API key.');
            }
            else if (resp.status === 'INVALID_REQUEST') {
                _this.showToastMsg('Invalid request. Please try again.');
            }
            else if (resp.status === 'UNKNOWN_ERROR') {
                _this.showToastMsg('An unknown error occured. Please try again.');
            }
        });
    };
    LivePage.prototype.showToastMsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        }).present();
    };
    LivePage.prototype.reCenterMe = function () {
        // console.log("getzoom level: " + this.allData.map.getCameraZoom());
        this.allData.map.moveCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            zoom: this.allData.map.getCameraZoom()
        }).then(function () {
        });
    };
    LivePage.prototype.getAddressTitle = function (marker) {
        var payload = {
            "lat": marker.getPosition().lat,
            "long": marker.getPosition().lng,
            "api_id": "1"
        };
        var addr;
        this.apiCall.getAddressApi(payload)
            .subscribe(function (data) {
            console.log("got address: " + data.results);
            if (data.results[2] != undefined || data.results[2] != null) {
                addr = data.results[2].formatted_address;
            }
            else {
                addr = 'N/A';
            }
            marker.setSnippet(addr);
        });
    };
    LivePage.prototype.addCluster = function (data) {
        var markerCluster = this.allData.map.addMarkerClusterSync({
            markers: data,
            icons: [
                {
                    min: 3,
                    max: 9,
                    url: "./assets/markercluster/small.png",
                    label: {
                        color: "white"
                    }
                },
                {
                    min: 10,
                    url: "./assets/markercluster/large.png",
                    label: {
                        color: "white"
                    }
                }
            ]
        });
        markerCluster.on(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (params) {
            var marker = params[1];
            marker.setTitle(marker.get("name"));
            marker.setSnippet(marker.get("address"));
            marker.showInfoWindow();
        });
    };
    LivePage.prototype.liveTrack = function (map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                mark.setIcon(icons);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.selectedVehicle != undefined) {
                        map.setCameraTarget(new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    }
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.selectedVehicle != undefined) {
                        map.setCameraTarget(dest);
                    }
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LivePage.prototype.temp = function (data) {
        // debugger
        var _this = this;
        if (data.status == 'Expired') {
            var profileModal = this.modalCtrl.create('ExpiredPage');
            profileModal.present();
            profileModal.onDidDismiss(function () {
                _this.selectedVehicle = undefined;
            });
        }
        else {
            var that = this;
            that.data = data;
            that.liveDataShare = data;
            // that.drawerHidden = true;
            that.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Bottom;
            that.onClickShow = false;
            if (that.allData.map != undefined) {
                // that.allData.map.clear();
                that.allData.map.remove();
            }
            console.log("on select change data=> " + JSON.stringify(data));
            for (var i = 0; i < that.socketChnl.length; i++)
                that._io.removeAllListeners(that.socketChnl[i]);
            that.allData = {};
            that.socketChnl = [];
            that.socketSwitch = {};
            if (data) {
                if (data.last_location) {
                    var mapOptions = {
                        // backgroundColor: 'white',
                        controls: {
                            compass: true,
                            zoom: false,
                            mapToolbar: true,
                            myLocation: true,
                        },
                        gestures: {
                            rotate: false,
                            tilt: false
                        },
                        mapType: that.mapKey
                    };
                    var map = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
                    map.animateCamera({
                        target: { lat: 20.5937, lng: 78.9629 },
                        zoom: 15,
                        duration: 1000,
                        padding: 0 // default = 20px
                    });
                    map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                    map.setPadding(20, 20, 20, 20);
                    // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                    that.allData.map = map;
                    that.socketInit(data);
                }
                else {
                    that.allData.map = that.newMap();
                    that.socketInit(data);
                }
                if (that.selectedVehicle != undefined) {
                    // that.drawerHidden = false;
                    that.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                    that.onClickShow = true;
                }
            }
            that.showBtn = true;
        }
    };
    LivePage.prototype.info = function (mark, cb) {
        mark.addListener('click', cb);
    };
    LivePage.prototype.setDocHeight = function () {
        var that = this;
        that.distanceTop = 200;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"D:\Pro\tcs2_gps\src\pages\live\live.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button *ngIf="showMenuBtn" ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-buttons start *ngIf="shwBckBtn">\n\n      <button ion-button small (click)="goBack()">\n\n        <ion-icon name="arrow-back"></ion-icon>{{ "Back" | translate }}\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>{{ "LIVE TRACKING" | translate }}\n\n      <span *ngIf="titleText">{{ "For" | translate }} {{ titleText }}</span>\n\n    </ion-title>\n\n    <ion-buttons end *ngIf="showBtn">\n\n      <button ion-button (click)="ngOnInit()">\n\n        {{ "All Vehicles" | translate }}\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content style="background-color: #f2f1f0">\n\n  <ion-item *ngIf="!titleText">\n\n    <ion-label>{{ SelectVehicle }}</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="temp(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <div id="map_canvas">\n\n    <ion-row *ngIf="selectedVehicle != undefined">\n\n      <ion-col>\n\n        <ion-fab top left>\n\n          <button ion-fab color="light" mini (click)="onClickMainMenu()">\n\n            <ion-icon color="gpsc" name="map"></ion-icon>\n\n          </button>\n\n          <ion-fab-list side="bottom">\n\n            <button style="margin: 0 2px;" ion-fab mini (click)="onClickMap(\'HYBRID\')" color="gpsc">\n\n              S\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n\n              T\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'NORMAL\')" color="gpsc">\n\n              N\n\n            </button>\n\n          </ion-fab-list>\n\n        </ion-fab>\n\n      </ion-col>\n\n      <ion-col>\n\n        <p class="blink" style="text-align: center;font-size:24px;color:cornflowerblue;font-weight: 600;"\n\n          *ngIf="!vehicle_speed">\n\n          0\n\n          <span style="font-size:16px;">{{ "Km/hr" | translate }}</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: green;font-weight: 600;"\n\n          *ngIf="vehicle_speed <= 60">\n\n          {{ vehicle_speed }}\n\n          <span style="font-size:16px;">{{ "Km/hr" | translate }}</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: red;font-weight: 600;"\n\n          *ngIf="vehicle_speed > 60">\n\n          {{ vehicle_speed }}\n\n          <span style="font-size:16px;">{{ "Km/hr" | translate }}</span>\n\n        </p>\n\n      </ion-col>\n\n      <ion-col>\n\n        <ion-fab top right>\n\n          <button ion-fab color="gpsc" mini>\n\n            <ion-icon name="arrow-dropdown"></ion-icon>\n\n          </button>\n\n          <ion-fab-list side="bottom">\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="shareLive($event)">\n\n              <ion-icon name="share"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapTrail\')">\n\n              <ion-icon name="eye"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapHideTraffic\')">\n\n              <ion-icon name="walk"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="settings()">\n\n              <ion-icon name="cog"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="addPOI()">\n\n              <ion-icon name="flag"></ion-icon>\n\n            </button>\n\n          </ion-fab-list>\n\n        </ion-fab>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-fab right style="margin-top:59%" *ngIf="selectedVehicle != undefined">\n\n      <button ion-fab color="gpsc" mini (click)="reCenterMe()">\n\n        <ion-icon name="locate"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab right style="margin-top:72%" *ngIf="selectedVehicle != undefined">\n\n      <button color="gpsc" ion-fab (click)="zoomin()" mini>\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab right style="margin-top:85%" *ngIf="selectedVehicle != undefined">\n\n      <button color="gpsc" ion-fab (click)="zoomout()" mini>\n\n        <ion-icon name="remove"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <!-- <ion-fab left style="margin-top:85%" *ngIf="selectedVehicle != undefined">\n\n      <button ion-fab color="gpsc" mini (click)="refreshMe()">\n\n        <ion-icon name="refresh"></ion-icon>\n\n      </button>\n\n    </ion-fab> -->\n\n\n\n    <ion-fab left style="margin-top:59%" *ngIf="selectedVehicle != undefined">\n\n      <button ion-fab color="gpsc" mini (click)="showNearby(\'police\')">\n\n        <ion-icon name="custom-station"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab left style="margin-top:72%" *ngIf="selectedVehicle != undefined">\n\n      <button ion-fab color="gpsc" mini (click)="showNearby(\'petrol\')">\n\n        <ion-icon name="custom-fuel"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab left style="margin-top:85%" *ngIf="selectedVehicle != undefined">\n\n      <button ion-fab color="gpsc" mini (click)="refreshMe1()">\n\n        <ion-icon name="refresh"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab top left *ngIf="selectedVehicle == undefined">\n\n      <button ion-fab color="light" mini>\n\n        <ion-icon color="gpsc" name="map"></ion-icon>\n\n      </button>\n\n      <ion-fab-list side="bottom">\n\n        <button ion-fab (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n\n          S\n\n        </button>\n\n        <button ion-fab (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n\n          T\n\n        </button>\n\n        <button ion-fab (click)="onClickMap(\'NORMAL\')" color="gpsc">\n\n          N\n\n        </button>\n\n      </ion-fab-list>\n\n    </ion-fab>\n\n\n\n    <ion-fab top right *ngIf="selectedVehicle == undefined">\n\n      <button ion-fab mini (click)="onSelectMapOption(\'mapHideTraffic\')"\n\n        color="{{ mapHideTraffic ? \'light\' : \'gpsc\' }}">\n\n        <ion-icon name="walk" color="{{ mapHideTraffic ? \'dark-grey\' : \'black\' }}"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 15%"\n\n      *ngIf="selectedVehicle == undefined">\n\n      <button ion-fab mini (click)="onSelectMapOption(\'showGeofence\')" color="gpsc">\n\n        <img src="assets/imgs/geo.png" />\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 28%"\n\n      *ngIf="selectedVehicle == undefined">\n\n      <button ion-fab mini (click)="getPOIs()" color="gpsc">\n\n        <ion-icon name="pin"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n  </div>\n\n</ion-content>\n\n<div *ngIf="onClickShow" class="divPlan">\n\n  <ion-bottom-drawer [(state)]="drawerState" [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce"\n\n    [distanceTop]="distanceTop" [minimumHeight]="minimumHeight" (click)="setDocHeight()">\n\n    <div class="drawer-content">\n\n      <p padding-left style="font-size: 13px; padding-bottom: 3px; color:cornflowerblue;">\n\n        {{ "Last Updated On" | translate }} &mdash;\n\n        {{ last_ping_on | date: "medium" }}\n\n      </p>\n\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n\n      <p padding-left style="font-size: 13px" *ngIf="address">{{ address }}</p>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC == null" width="20" height="20" />\n\n          <p>{{ "IGN" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="acModel == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="acModel == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="acModel == \'0\'" width="20" height="20" />\n\n          <p>{{ "AC" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20" />\n\n          <p>{{ "FUEL" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power == \'1\'" width="20" height="20" />\n\n          <p>{{ "POWER" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking == null" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking == \'0\'" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking == \'1\'" width="30" height="20" />\n\n          <p>{{ "GPS" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!total_odo">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="total_odo">\n\n            {{ total_odo | number: "1.0-2" }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Odometer" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!vehicle_speed">\n\n            0{{ "km/h" | translate }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="vehicle_speed">\n\n            {{ vehicle_speed }}{{ "km/h" | translate }}\n\n          </p>\n\n          <p style="font-size: 13px">\n\n            {{ "Speed" | translate }}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="fuel">\n\n            {{ fuel }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!fuel">N/A</p>\n\n          <p style="font-size: 13px">{{ "Fuel" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!distFromLastStop">\n\n            0{{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="distFromLastStop">\n\n            {{ distFromLastStop | number: "1.0-2" }}{{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!todays_odo">\n\n            0{{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="todays_odo">\n\n            {{ todays_odo | number: "1.0-2" }}{{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!timeAtLastStop">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="timeAtLastStop">\n\n            {{ timeAtLastStop }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "At Last Stop" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_stopped">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_stopped">\n\n            {{ today_stopped }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!lastStoppedAt">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="lastStoppedAt">\n\n            {{ lastStoppedAt }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_running">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_running">\n\n            {{ today_running }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>'/*ion-inline-end:"D:\Pro\tcs2_gps\src\pages\live\live.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_11__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], LivePage);
    return LivePage;
}());

//# sourceMappingURL=live.js.map

/***/ }),

/***/ 598:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivePageModule", function() { return LivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__live__ = __webpack_require__(1051);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var LivePageModule = /** @class */ (function () {
    function LivePageModule() {
    }
    LivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], LivePageModule);
    return LivePageModule;
}());

//# sourceMappingURL=live.module.js.map

/***/ })

});
//# sourceMappingURL=30.js.map