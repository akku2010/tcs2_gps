webpackJsonp([40],{

/***/ 548:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FastagPageModule", function() { return FastagPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fastag__ = __webpack_require__(992);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FastagPageModule = /** @class */ (function () {
    function FastagPageModule() {
    }
    FastagPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fastag__["a" /* FastagPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fastag__["a" /* FastagPage */]),
            ],
        })
    ], FastagPageModule);
    return FastagPageModule;
}());

//# sourceMappingURL=fastag.module.js.map

/***/ }),

/***/ 992:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FastagPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FastagPage = /** @class */ (function () {
    function FastagPage(navCtrl, navParams, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.buttonColor = '#fff'; //Default Color
        this.buttonColor1 = '#fff';
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    FastagPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FastagPage');
    };
    FastagPage.prototype.addEvent = function () {
        this.vehicleType = 'Truck';
        this.buttonColor1 = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent1 = function () {
        this.vehicleType = 'Bus';
        this.buttonColor = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor1 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent2 = function () {
        this.vehicleType = 'Taxi';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor2 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent3 = function () {
        this.vehicleType = 'Car';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor3 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent4 = function () {
        this.vehicleType = 'Bike';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor4 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent5 = function () {
        this.vehicleType = 'Other';
        this.buttonColor4 = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor5 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.fastagReq = function () {
        var _this = this;
        if (this.vehicleType === undefined || this.numFastag === undefined) {
            this.toastCtrl.create({
                message: 'Please select the vehicle type and add number of requests.',
                duration: 2000,
                position: 'bottom'
            }).present();
            return;
        }
        var url = this.apiCall.mainUrl + "fastTag/addRequest";
        var payload = {};
        // debugger
        if (this.islogin.Dealer_ID === undefined) {
            payload = {
                user: this.islogin._id,
                Dealer: this.islogin.supAdmin,
                supAdmin: this.islogin.supAdmin,
                date: new Date().toISOString(),
                quantity: this.numFastag,
                vehicle_type: this.vehicleType
            };
        }
        else {
            payload = {
                user: this.islogin._id,
                Dealer: this.islogin.Dealer_ID._id,
                supAdmin: this.islogin.supAdmin,
                date: new Date().toISOString(),
                quantity: this.numFastag,
                vehicle_type: this.vehicleType
            };
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, payload)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log('response data: ', respData);
            if (respData.message === 'saved succesfully') {
                _this.toastCtrl.create({
                    message: 'Fastag added successfully.',
                    duration: 2000,
                    position: 'bottom'
                }).present();
                _this.navCtrl.pop();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FastagPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fastag',template:/*ion-inline-start:"D:\Pro\tcs2_gps\src\pages\fastag-list\fastag\fastag.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Add Fastag</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-row>\n\n    <ion-col>Choose Vehicle Type</ion-col>\n\n  </ion-row>\n\n  <ion-row>\n\n    <ion-col col-4 (click)="addEvent();">\n\n      <ion-card [ngStyle]="{\'background-color\': buttonColor}">\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/Truck.png" />\n\n          </ion-col>\n\n          <ion-col col-12 text-center>TRUCK</ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </ion-col>\n\n    <ion-col col-4 (click)="addEvent1();">\n\n      <ion-card [ngStyle]="{\'background-color\': buttonColor1}">\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/bus.png" />\n\n          </ion-col>\n\n          <ion-col col-12 text-center>BUS</ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </ion-col>\n\n    <ion-col col-4 (click)="addEvent2();">\n\n      <ion-card [ngStyle]="{\'background-color\': buttonColor2}">\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/cab.png" />\n\n          </ion-col>\n\n          <ion-col col-12 text-center>TAXI</ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </ion-col>\n\n  </ion-row>\n\n  <ion-row>\n\n    <ion-col col-4 (click)="addEvent3();">\n\n      <ion-card [ngStyle]="{\'background-color\': buttonColor3}">\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/car.png" />\n\n          </ion-col>\n\n          <ion-col col-12 text-center>CAR</ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </ion-col>\n\n    <ion-col col-4 (click)="addEvent4();">\n\n      <ion-card [ngStyle]="{\'background-color\': buttonColor4}">\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/motorcycle.png" />\n\n          </ion-col>\n\n          <ion-col col-12 text-center>BIKE</ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </ion-col>\n\n    <ion-col col-4 (click)="addEvent5();">\n\n      <ion-card [ngStyle]="{\'background-color\': buttonColor5}">\n\n        <ion-row>\n\n          <ion-col col-12>\n\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/Truck.png" />\n\n          </ion-col>\n\n          <ion-col col-12 text-center>OTHERS</ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </ion-col>\n\n  </ion-row>\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-input type="number" [(ngModel)]="numFastag" placeholder="Enter number of FasTags required">\n\n\n\n      </ion-input>\n\n    </ion-item>\n\n  </ion-list>\n\n  <ion-row>\n\n    <ion-col>\n\n      <button ion-button block color="gpsc" (click)="fastagReq()">Send Rquest</button>\n\n\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\tcs2_gps\src\pages\fastag-list\fastag\fastag.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], FastagPage);
    return FastagPage;
}());

//# sourceMappingURL=fastag.js.map

/***/ })

});
//# sourceMappingURL=40.js.map