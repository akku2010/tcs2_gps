webpackJsonp([53],{

/***/ 1034:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCustomerModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var AddCustomerModal = /** @class */ (function () {
    function AddCustomerModal(navCtrl, navParams, formBuilder, apicallCustomer, alerCtrl, viewCtrl, toastCtrl, actionSheetCtrl, file, filePath, camera, transferObj, transfer, loadingCtrl, platform, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.apicallCustomer = apicallCustomer;
        this.alerCtrl = alerCtrl;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.file = file;
        this.filePath = filePath;
        this.camera = camera;
        this.transferObj = transferObj;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.translate = translate;
        this.customerdata = {};
        // isDealer: any;
        this.DlType = [{
                value: 'dl',
                viewValue: "Driving License"
            }, {
                value: 'Adhar',
                viewValue: "Adhar Card"
            }, {
                value: 'PAN',
                viewValue: "PAN Card"
            }, {
                value: 'voterCard',
                viewValue: "Voter ID Card"
            }
        ];
        this.lastImage = null;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        this.currentYear = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        this.addcustomerform = formBuilder.group({
            userId: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            Firstname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            LastName: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            emailid: [this.islogin.account, [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email]],
            contact_num: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].maxLength(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(10)]],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            confpassword: [''],
            address: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            ExipreDate: [this.currentYear, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            dealer_firstname: [''],
            DlNo: [""],
            Name: [""]
        });
    }
    Object.defineProperty(AddCustomerModal.prototype, "frm", {
        get: function () { return this.addcustomerform.controls; },
        enumerable: true,
        configurable: true
    });
    AddCustomerModal.prototype.ngOnInit = function () {
        if (this.isSuperAdminStatus) {
            this.getAllDealers();
        }
    };
    AddCustomerModal.prototype.DocumentOnChnage = function (type) {
        this.Documentdata = type;
        if (this.Documentdata.value == 'dl') {
            this.Documentdatashow = this.Documentdata.value;
        }
        else if (this.Documentdata.value == 'Adhar') {
            this.DocumentdataAdhar = this.Documentdata.value;
        }
    };
    // Create a new name for the image
    AddCustomerModal.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    AddCustomerModal.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    AddCustomerModal.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    AddCustomerModal.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    AddCustomerModal.prototype.uploadImage = function () {
        var _this = this;
        // Destination URL
        var url = "https://www.oneqlik.in/users/uploadImage";
        var targetPath = this.pathForImage(this.lastImage);
        console.log("TargetPath=>", targetPath);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "photo",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpeg",
            params: { 'fileName': filename }
        };
        // multipart/form-data"
        this.transferObj = this.transfer.create();
        this.Imgloading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.Imgloading.present();
        this.transferObj.upload(targetPath, url, options).then(function (data) {
            _this.Imgloading.dismissAll();
        }, function (err) {
            console.log("uploadError=>", err);
            _this.lastImage = null;
            _this.Imgloading.dismissAll();
            _this.presentToast('Error while uploading file, Please try again !!!');
        });
    };
    AddCustomerModal.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    AddCustomerModal.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    AddCustomerModal.prototype.dealerOnChnage = function (dealer) {
        console.log(dealer);
        this.dealerdata = dealer;
        console.log("dealer id=> " + this.dealerdata.dealer_id);
    };
    AddCustomerModal.prototype.addcustomer = function () {
        var _this = this;
        this.submitAttempt = true;
        // console.log(devicedetails);
        if (this.addcustomerform.valid) {
            debugger;
            if (this.islogin.isSuperAdmin == true) {
                this.customerdata = {
                    "first_name": this.addcustomerform.value.Firstname,
                    "last_name": this.addcustomerform.value.LastName,
                    "email": this.addcustomerform.value.emailid,
                    "phone": this.addcustomerform.value.contact_num,
                    "password": this.addcustomerform.value.password,
                    "isDealer": false,
                    "custumer": true,
                    "status": true,
                    "user_id": this.addcustomerform.value.userId,
                    "address": this.addcustomerform.value.address,
                    "supAdmin": this.islogin._id
                };
            }
            else {
                if (this.islogin.isDealer == true) {
                    this.customerdata = {
                        "first_name": this.addcustomerform.value.Firstname,
                        "last_name": this.addcustomerform.value.LastName,
                        "email": this.addcustomerform.value.emailid,
                        "phone": this.addcustomerform.value.contact_num,
                        "password": this.addcustomerform.value.password,
                        "isDealer": this.islogin.isDealer,
                        "custumer": true,
                        "status": true,
                        "user_id": this.addcustomerform.value.userId,
                        "address": this.addcustomerform.value.address,
                        "supAdmin": this.islogin.supAdmin,
                    };
                }
            }
            if (this.dealerdata != undefined) {
                this.customerdata.Dealer = this.dealerdata.dealer_id;
            }
            else {
                this.customerdata.Dealer = this.islogin._id;
            }
            this.apicallCustomer.startLoading().present();
            this.apicallCustomer.signupApi(this.customerdata)
                .subscribe(function (data) {
                _this.apicallCustomer.stopLoading();
                _this.Customeradd = data;
                var toast = _this.toastCtrl.create({
                    message: _this.translate.instant('dealeradded', { value: _this.translate.instant('cust') }),
                    position: 'top',
                    duration: 1500
                });
                toast.onDidDismiss(function () {
                    _this.viewCtrl.dismiss();
                });
                toast.present();
            }, function (err) {
                _this.apicallCustomer.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var namepass = [];
                namepass = msg.split(":");
                var name = namepass[1];
                var alert = _this.alerCtrl.create({
                    message: name,
                    buttons: [_this.translate.instant('Okay')]
                });
                alert.present();
            });
        }
    };
    AddCustomerModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddCustomerModal.prototype.getAllDealers = function () {
        var _this = this;
        var baseURLp = this.apicallCustomer.mainUrl + 'users/getAllDealerVehicles?supAdmin=' + this.islogin._id;
        var toast = this.toastCtrl.create({
            message: 'Loading dealers..',
            position: 'bottom',
            duration: 1500
        });
        toast.present();
        this.apicallCustomer.getAllDealerCall(baseURLp)
            .subscribe(function (data) {
            _this.selectDealer = data;
            // toast.dismiss();
        }, function (error) {
            console.log(error);
        });
    };
    AddCustomerModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-customer-model',template:/*ion-inline-start:"D:\Pro\tcs2_gps\src\pages\customers\modals\add-customer-modal\add-customer-modal.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{\'Add Customer\' | translate}}</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="addcustomerform">\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'User ID*\' | translate}}</ion-label>\n\n            <ion-input formControlName="userId" type="text"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addcustomerform.controls.userId.valid && (addcustomerform.controls.userId.dirty || submitAttempt)">\n\n            <p>{{\'user id required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'First Name*\' | translate}}</ion-label>\n\n            <ion-input formControlName="Firstname" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addcustomerform.controls.Firstname.valid && (addcustomerform.controls.Firstname.dirty || submitAttempt)">\n\n            <p>{{\'first name required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Last Name*\' | translate}}</ion-label>\n\n            <ion-input formControlName="LastName" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addcustomerform.controls.LastName.valid && (addcustomerform.controls.LastName.dirty || submitAttempt)">\n\n            <p>{{\'last name required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Email ID*\'|translate}}</ion-label>\n\n            <ion-input formControlName="emailid" type="email"></ion-input>\n\n        </ion-item>\n\n        <!-- <ion-item class="logitem1" *ngIf="!addcustomerform.controls.emailid.valid && (addcustomerform.controls.emailid.dirty || submitAttempt)">\n\n            <p>{{\'email id required!\' | translate}}</p>\n\n        </ion-item> -->\n\n        <div *ngIf="submitAttempt && frm.emailid.errors" class="logitem1">\n\n            <ion-item *ngIf="frm.emailid.errors.required">\n\n                <p class="logitem1">\n\n                    <ion-icon name="information-circle"></ion-icon>&nbsp;&nbsp;Email is required!\n\n                </p>\n\n            </ion-item>\n\n            <ion-item *ngIf="frm.emailid.errors.email">\n\n                <p class="logitem1">\n\n                    <ion-icon name="information-circle"></ion-icon>&nbsp;&nbsp;Email must be a valid email address!\n\n                </p>\n\n            </ion-item>\n\n        </div>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Mobile Number\' | translate}}</ion-label>\n\n            <ion-input formControlName="contact_num" type="tel" maxlength="10" minlength="10"></ion-input>\n\n        </ion-item>\n\n        <div *ngIf="submitAttempt && frm.contact_num.errors" class="logitem1">\n\n            <ion-item *ngIf="frm.contact_num.errors.required">\n\n                <p class="logitem1">\n\n                    <ion-icon name="information-circle"></ion-icon>&nbsp;&nbsp;Mobile number is required!\n\n                </p>\n\n            </ion-item>\n\n            <ion-item *ngIf="frm.contact_num.errors.minlength">\n\n                <p class="logitem1">\n\n                    <ion-icon name="information-circle"></ion-icon>&nbsp;&nbsp;Mobile number must be 10 digits!\n\n                </p>\n\n            </ion-item>\n\n        </div>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Password*\' | translate}}</ion-label>\n\n            <ion-input formControlName="password" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addcustomerform.controls.password.valid && (addcustomerform.controls.password.dirty || submitAttempt)">\n\n            <p>{{\'Password required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Address*\' | translate}}</ion-label>\n\n            <ion-input formControlName="address" type="text"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addcustomerform.controls.address.valid && (addcustomerform.controls.address.dirty || submitAttempt)">\n\n            <p>{{\'Address required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{\'Expire On\' | translate}}</ion-label>\n\n            <ion-input type="date" formControlName="ExipreDate" style="margin-left: -2px;" min="{{minDate}}">\n\n            </ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1"\n\n            *ngIf="!addcustomerform.controls.ExipreDate.valid && (addcustomerform.controls.ExipreDate.dirty || submitAttempt)">\n\n            <p>{{\'Date of expiry required!\' | translate}}</p>\n\n        </ion-item>\n\n\n\n        <ion-item *ngIf="isSuperAdminStatus">\n\n            <ion-label>{{\'Dealers\' | translate}}</ion-label>\n\n            <ion-select formControlName="dealer_firstname" style="min-width:49%;">\n\n                <ion-option *ngFor="let dealer of selectDealer" [value]="dealer.dealer_firstname"\n\n                    (ionSelect)="dealerOnChnage(dealer)">{{dealer.dealer_firstname | titlecase}}\n\n                    {{dealer.dealer_lastname | titlecase}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n    </form>\n\n</ion-content>\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row no-padding>\n\n            <ion-col width-50 style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="addcustomer()">{{\'ADD CUSTOMER\' | translate}}</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\Pro\tcs2_gps\src\pages\customers\modals\add-customer-modal\add-customer-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["b" /* TransferObject */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["c" /* TranslateService */]])
    ], AddCustomerModal);
    return AddCustomerModal;
}());

//# sourceMappingURL=add-customer-modal.js.map

/***/ }),

/***/ 581:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCustomerModalModule", function() { return AddCustomerModalModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_customer_modal__ = __webpack_require__(1034);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_path__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AddCustomerModalModule = /** @class */ (function () {
    function AddCustomerModalModule() {
    }
    AddCustomerModalModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_customer_modal__["a" /* AddCustomerModal */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_customer_modal__["a" /* AddCustomerModal */]),
                __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["b" /* TransferObject */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["b" /* FileTransferObject */],
            ]
        })
    ], AddCustomerModalModule);
    return AddCustomerModalModule;
}());

//# sourceMappingURL=add-customer-modal.module.js.map

/***/ })

});
//# sourceMappingURL=53.js.map