webpackJsonp([55],{

/***/ 1032:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateTripPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_api_service_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CreateTripPage = /** @class */ (function () {
    function CreateTripPage(toastCtrl, geoLocation, apiCall, nativeGeocoder, event, navCtrl, navParams, storage) {
        this.toastCtrl = toastCtrl;
        this.geoLocation = geoLocation;
        this.apiCall = apiCall;
        this.nativeGeocoder = nativeGeocoder;
        this.event = event;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.autocompleteItems = [];
        this.autocomplete = {};
        this.newLat = 0;
        this.newLng = 0;
        this.tripData = {};
        this.deviceDetails = {};
        this.service = new google.maps.DistanceMatrixService();
        this._commonVar = {};
        this.expectation = {};
        this.showBtn = false;
        console.log("Param data: ", navParams.get("paramData"));
        console.log("trip data: ", navParams.get("tripData"));
        this.deviceDetails = navParams.get("paramData");
        this.tripData = navParams.get("tripData");
        this.acService = new google.maps.places.AutocompleteService();
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
    }
    CreateTripPage.prototype.ngOnInit = function () {
        this.autocompleteItems = [];
        this.autocomplete = {
            query: '',
            yourLocation: 'N/A'
        };
        // debugger
        if (this.deviceDetails.last_location != undefined) {
            this.drawGeofence(this.deviceDetails.last_location['lat'], this.deviceDetails.last_location['long']);
            var that_1 = this;
            var payload = {
                "lat": this.deviceDetails.last_location['lat'],
                "long": this.deviceDetails.last_location['long'],
                "api_id": "1"
            };
            this.apiCall.getAddressApi(payload)
                .subscribe(function (data) {
                // console.log("got address: "+ data.results)
                if (data.results[2] != undefined || data.results[0] != null) {
                    that_1.autocomplete.yourLocation = data.results[0].formatted_address;
                }
                else {
                    that_1.autocomplete.yourLocation = 'N/A';
                }
            });
        }
    };
    CreateTripPage.prototype.ngOnDestroy = function () {
        if (localStorage.getItem("travelDetailsObject") != null) {
            localStorage.removeItem("travelDetailsObject");
        }
    };
    CreateTripPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateTripPage');
    };
    CreateTripPage.prototype.updateSearch = function () {
        // debugger
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var that = this;
        var config = {
            //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
            input: that.autocomplete.query,
            componentRestrictions: {}
        };
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            console.log("lat long not find ", predictions);
            that.autocompleteItems = [];
            predictions.forEach(function (prediction) {
                that.autocompleteItems.push(prediction);
            });
            console.log("autocompleteItems=> " + that.autocompleteItems);
        });
    };
    CreateTripPage.prototype.chooseItem = function (item) {
        var _this = this;
        var that = this;
        that.autocomplete.query = item.description;
        console.log("console items=> " + JSON.stringify(item));
        that.autocompleteItems = [];
        var options = {
            useLocale: true,
            maxResults: 5
        };
        that.apiCall.startLoading().present();
        this.nativeGeocoder.forwardGeocode(item.description, options)
            .then(function (coordinates) {
            console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude);
            that.newLat = coordinates[0].latitude;
            that.newLng = coordinates[0].longitude;
            var dest = new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["f" /* LatLng */](parseFloat(that.newLat), parseFloat(that.newLng));
            var sources = new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["f" /* LatLng */](that.deviceDetails.last_location.lat, that.deviceDetails.last_location.long);
            that.calcRoute(sources, dest);
            _this.map.addMarker({
                title: 'Source',
                icon: 'green',
                position: sources,
            }).then(function (data) {
                console.log("Marker added");
            });
        })
            .catch(function (error) { return console.log(error); });
    };
    CreateTripPage.prototype.setDestination = function () {
        var _this = this;
        var url = "https://www.oneqlik.in/user_trip/planTrip";
        // var url = "https://www.oneqlik.in/user_trip/updatePlantrip";
        if (this.tripName == undefined) {
            var toast = this.toastCtrl.create({
                message: 'Please enter the trip name.',
                duration: 1500,
                position: 'middle'
            });
            toast.present();
        }
        else {
            var payload = {
                "user": this.userdetails._id,
                "device": this.deviceDetails._id,
                "start_loc": {
                    "lat": this.deviceDetails.last_location.lat,
                    "long": this.deviceDetails.last_location.long
                },
                "trip_status": 'Started',
                "end_loc": {
                    "lat": this.newLat,
                    "long": this.newLng
                },
                "trip_name": this.tripName,
                "start_time": new Date().toISOString()
            };
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, payload)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("resceved data: ", data);
                var toast = _this.toastCtrl.create({
                    message: 'Trip has been created successfully.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
                var that = _this;
                if (data.message == 'Trip Created') {
                    _this.storage.set("TRIPDATA", data).then(function (res) {
                        console.log("ionic storage res: ", res);
                        that.event.publish("tripstatUpdated", data.message);
                        that.navCtrl.pop();
                    });
                }
            }, function (err) {
                _this.apiCall.stopLoading();
            });
        }
    };
    CreateTripPage.prototype.drawGeofence = function (lat, lng) {
        var _this = this;
        if (this.map != undefined) {
            this.map.remove();
        }
        this.mapElement = document.getElementById('mapTrip');
        console.log(this.mapElement);
        this.map = __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["b" /* GoogleMaps */].create(this.mapElement);
        // Wait the MAP_READY before using any methods.
        this.map.one(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MAP_READY)
            .then(function () {
            console.log('Map is ready!');
            var pos = {
                target: new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["f" /* LatLng */](lat, lng),
                zoom: 12,
                tilt: 30
            };
            _this.map.moveCamera(pos);
            _this.map.addMarker({
                title: '',
                position: new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["f" /* LatLng */](lat, lng),
            }).then(function (data) {
                console.log("Marker added");
                _this.newLat = lat;
                _this.newLng = lng;
            });
            // });
        });
    };
    CreateTripPage.prototype.calcRoute = function (start, end) {
        this._commonVar.AIR_PORTS = [];
        var directionsService = new google.maps.DirectionsService();
        var that = this;
        var request = {
            origin: start,
            destination: end,
            // waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var path = new google.maps.MVCArray();
                for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
                    path.push(response.routes[0].overview_path[i]);
                    that._commonVar.AIR_PORTS.push({
                        lat: path.j[i].lat(), lng: path.j[i].lng()
                    });
                    if (that._commonVar.AIR_PORTS.length > 1) {
                        that.map.addMarker({
                            title: 'Destination',
                            position: end,
                            icon: 'red'
                        });
                        that.map.addPolyline({
                            'points': that._commonVar.AIR_PORTS,
                            'color': '#4aa9d5',
                            'width': 4,
                            'geodesic': true,
                        }).then(function () {
                            that.getTravelDetails(start, end);
                            that.showBtn = true;
                        });
                    }
                }
                var bounds = new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["g" /* LatLngBounds */](that._commonVar.AIR_PORTS);
                that.map.moveCamera({
                    target: bounds
                });
                that.apiCall.stopLoading();
                // that.socketInit(that._commonVar._data);
            }
        });
    };
    CreateTripPage.prototype.getTravelDetails = function (source, dest) {
        var _this = this;
        var that = this;
        this._id = setInterval(function () {
            if (localStorage.getItem("travelDetailsObject") != null) {
                if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
                    // if (that.expectation == undefined) {
                    that.expectation = JSON.parse(localStorage.getItem("travelDetailsObject"));
                    console.log("expectation: ", that.expectation);
                }
                else {
                    clearInterval(_this._id);
                }
            }
        }, 3000);
        that.service.getDistanceMatrix({
            origins: [source],
            destinations: [dest],
            travelMode: 'DRIVING'
        }, that.callback);
    };
    CreateTripPage.prototype.callback = function (response, status) {
        var travelDetailsObject;
        if (status == 'OK') {
            var origins = response.originAddresses;
            for (var i = 0; i < origins.length; i++) {
                var results = response.rows[i].elements;
                for (var j = 0; j < results.length; j++) {
                    var element = results[j];
                    var distance = element.distance.text;
                    var duration = element.duration.text;
                    travelDetailsObject = {
                        distance: distance,
                        duration: duration
                    };
                }
            }
            localStorage.setItem("travelDetailsObject", JSON.stringify(travelDetailsObject));
        }
    };
    CreateTripPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-create-trip',template:/*ion-inline-start:"D:\Pro\tcs2_gps\src\pages\create-trip\create-trip.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{ "Create Trip" | translate }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div #mapTrip id="mapTrip" data-tap-disabled="true">\n\n    <div\n\n      style="padding-left: 8px; padding-right: 8px; padding-top: 5px; padding-bottom: 0px;"\n\n    >\n\n      <ion-row class="rowsty">\n\n        <ion-col col-1>\n\n          <ion-icon\n\n            style="font-size: 1.5em; color: gray;"\n\n            name="car"\n\n          ></ion-icon>\n\n        </ion-col>\n\n        <ion-col\n\n          col-11\n\n          style="padding-right: 5px; padding-left: 0px; padding-top: 0px; padding-bottom: 0px;"\n\n        >\n\n          <input\n\n            type="text"\n\n            class="searchbar-input"\n\n            placeholder="{{ \'Enter trip name\' | translate }}"\n\n            name="tripName"\n\n            [(ngModel)]="tripName"\n\n          />\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n\n\n    <ion-searchbar\n\n      class="search_bar"\n\n      [(ngModel)]="autocomplete.yourLocation"\n\n      placeholder="{{ \'Your location\' | translate }}"\n\n    >\n\n    </ion-searchbar>\n\n\n\n    <ion-searchbar\n\n      class="search_bar"\n\n      [(ngModel)]="autocomplete.query"\n\n      (ionInput)="updateSearch()"\n\n      placeholder="{{ \'Where to?\' | translate }}"\n\n    >\n\n    </ion-searchbar>\n\n    <ion-list style="margin: 0px;">\n\n      <ion-item\n\n        *ngFor="let item of autocompleteItems"\n\n        (click)="chooseItem(item)"\n\n      >\n\n        {{ item.description }}\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n    <ion-row\n\n      *ngIf="expectation.distance"\n\n      style="background-color: rgb(0, 0, 0, 0.5); font-size: 0.8em; color: white;border-radius: 25px;width: 70%;margin: auto; padding:5px;"\n\n    >\n\n      <ion-col style="background-color: transparent; text-align: center;" col-6>\n\n        {{ "Distance" | translate }} {{ expectation.distance }}\n\n      </ion-col>\n\n      <ion-col style="background-color: transparent; text-align: center;" col-6>\n\n        {{ "Time" | translate }} {{ expectation.duration }}\n\n      </ion-col>\n\n    </ion-row>\n\n  </div>\n\n</ion-content>\n\n<ion-footer class="footSty" *ngIf="showBtn">\n\n  <ion-toolbar>\n\n    <ion-row no-padding>\n\n      <ion-col width-50 style="text-align: center;">\n\n        <button ion-button clear color="light" (click)="setDestination()">\n\n          {{ "Start Trip" | translate }}\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"D:\Pro\tcs2_gps\src\pages\create-trip\create-trip.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_5__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]])
    ], CreateTripPage);
    return CreateTripPage;
}());

//# sourceMappingURL=create-trip.js.map

/***/ }),

/***/ 579:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateTripPageModule", function() { return CreateTripPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_trip__ = __webpack_require__(1032);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CreateTripPageModule = /** @class */ (function () {
    function CreateTripPageModule() {
    }
    CreateTripPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__create_trip__["a" /* CreateTripPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__create_trip__["a" /* CreateTripPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], CreateTripPageModule);
    return CreateTripPageModule;
}());

//# sourceMappingURL=create-trip.module.js.map

/***/ })

});
//# sourceMappingURL=55.js.map