webpackJsonp([14],{

/***/ 542:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceSummaryRepoPageModule", function() { return DeviceSummaryRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__device_summary_repo__ = __webpack_require__(984);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dummy_directive__ = __webpack_require__(985);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var DeviceSummaryRepoPageModule = /** @class */ (function () {
    function DeviceSummaryRepoPageModule() {
    }
    DeviceSummaryRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__device_summary_repo__["a" /* DeviceSummaryRepoPage */],
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__device_summary_repo__["a" /* DeviceSummaryRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
        })
    ], DeviceSummaryRepoPageModule);
    return DeviceSummaryRepoPageModule;
}());

//# sourceMappingURL=device-summary-repo.module.js.map

/***/ }),

/***/ 984:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceSummaryRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DeviceSummaryRepoPage = /** @class */ (function () {
    function DeviceSummaryRepoPage(navCtrl, navParams, apicallsummary, toastCtrl, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallsummary = apicallsummary;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.summaryReport = [];
        this.summaryReportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // var yestDay = moment().subtract(1, 'days');
        // console.log("yesterdays date: ", yestDay);
        console.log("yest time: ", __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).subtract(1, 'days').format());
        // this.datetimeStart = moment({ hours: 0 }).format();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
        console.log("today time: ", this.datetimeStart);
        // this.datetimeEnd = moment().format();//new Date(a).toISOString();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format(); // today date and time with 12:00am
    }
    DeviceSummaryRepoPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    DeviceSummaryRepoPage.prototype.getSummaarydevice = function (selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.device_id = selectedVehicle.Device_ID;
    };
    DeviceSummaryRepoPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apicallsummary.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicallsummary.startLoading().present();
        this.apicallsummary.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicallsummary.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicallsummary.stopLoading();
            console.log(err);
        });
    };
    DeviceSummaryRepoPage.prototype.getSummaryReport = function () {
        var _this = this;
        var that = this;
        this.summaryReport = [];
        this.summaryReportData = [];
        if (this.device_id == undefined) {
            this.device_id = "";
        }
        this.apicallsummary.startLoading().present();
        this.apicallsummary.getSummaryReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.islogin._id, this.device_id)
            .subscribe(function (data) {
            _this.apicallsummary.stopLoading();
            _this.summaryReport = data;
            console.log(data);
            if (_this.summaryReport.length > 0) {
                _this.innerFunc(_this.summaryReport);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Report(s) not found for selected dates/vehicle.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicallsummary.stopLoading();
            console.log(error);
        });
    };
    DeviceSummaryRepoPage.prototype.innerFunc = function (summaryReport) {
        var outerthis = this;
        var i = 0, howManyTimes = summaryReport.length;
        function f() {
            // console.log("conversion: ", Number(outerthis.summaryReport[i].devObj[0].Mileage))
            // var hourconversion = 2.7777778 / 10000000;
            outerthis.summaryReportData.push({
                'Device_Name': summaryReport[i].devObj[0].Device_Name,
                'routeViolations': summaryReport[i].today_routeViolations,
                'overspeeds': outerthis.millisecondConversion(summaryReport[i].today_overspeeds),
                'ignOn': outerthis.millisecondConversion(summaryReport[i].today_running),
                'ignOff': outerthis.millisecondConversion(summaryReport[i].today_stopped),
                'distance': summaryReport[i].today_odo,
                'tripCount': summaryReport[i].today_trips,
                'mileage': ((summaryReport[i].devObj[0].Mileage == null) || (summaryReport[i].devObj[0].Mileage == undefined)) ? "NA" : (summaryReport[i].today_odo / parseFloat(summaryReport[i].devObj[0].Mileage)).toFixed(2),
                'end_location': summaryReport[i].end_location,
                'start_location': summaryReport[i].start_location,
                't_ofr': outerthis.millisecondConversion(summaryReport[i].t_ofr),
                't_idling': outerthis.millisecondConversion(summaryReport[i].t_idling)
            });
            outerthis.start_address(summaryReport[i], i);
            outerthis.end_address(summaryReport[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    DeviceSummaryRepoPage.prototype.millisecondConversion = function (duration) {
        var minutes = Math.floor((duration / (1000 * 60)) % 60);
        var hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
        hours = (hours < 10) ? 0 + hours : hours;
        minutes = (minutes < 10) ? 0 + minutes : minutes;
        return hours + ":" + minutes;
    };
    DeviceSummaryRepoPage.prototype.start_address = function (item, index) {
        var that = this;
        that.summaryReportData[index].StartLocation = "N/A";
        if (item.start_location == null || item.start_location == undefined) {
            that.summaryReportData[index].StartLocation = "N/A";
        }
        else if (item.start_location != null || item.end_location != undefined) {
            this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
                .then(function (res) {
                console.log("test", res);
                console.log("check lat: " + item.start_location.lat);
                console.log("check long: " + item.start_location.long);
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
                that.summaryReportData[index].StartLocation = str;
            });
        }
    };
    DeviceSummaryRepoPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apicallsummary.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    DeviceSummaryRepoPage.prototype.end_address = function (item, index) {
        var that = this;
        that.summaryReportData[index].EndLocation = "N/A";
        if (item.end_location == null || item.end_location == undefined) {
            that.summaryReportData[index].EndLocation = "N/A";
        }
        else if (item.end_location != null || item.end_location != undefined) {
            this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
                that.summaryReportData[index].EndLocation = str;
            });
        }
    };
    DeviceSummaryRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-device-summary-repo',template:/*ion-inline-start:"D:\Pro\tcs2_gps\src\pages\device-summary-repo\device-summary-repo.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ "Summary Report" | translate }}</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>{{ "Select Vehicle" | translate }}</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getSummaarydevice(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getSummaryReport()">\n\n        </ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-card *ngFor="let item of summaryReportData; let i = index">\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <!-- <ion-avatar item-start>\n\n        <img src="assets/imgs/car2.png" />\n\n      </ion-avatar>\n\n      <p style="color:black; font-size:16px; padding-left: 4px;">\n\n        {{ item.Device_Name }}\n\n      </p> -->\n\n      <ion-row>\n\n        <ion-col col-3>\n\n          <img src="assets/imgs/car2.png" style="\n\n            margin-top: -10px; \n\n            width: 40px; \n\n            height: 40px;" />\n\n        </ion-col>\n\n        <ion-col col-5>\n\n          <p style="\n\n            color:black; \n\n            font-size:16px;\n\n            margin: 0px;">\n\n            {{ item.Device_Name }}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <p style="\n\n            text-align: center;\n\n            font-size: 11px;\n\n            color:#11c1f3;  \n\n            font-weight:bold;">\n\n            {{"Trips" | translate}} - {{item.tripCount}}\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row style="margin-top:2%;">\n\n        <ion-col col-3>\n\n          <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;">\n\n            <span *ngIf="item.ignOn">{{ item.ignOn }}</span>\n\n            <span *ngIf="!item.ignOn">00.00</span>&nbsp;\n\n          </p>\n\n\n\n          <p style="\n\n          color:#53ab53;\n\n          font-size:11px;\n\n          font-weight: bold;">\n\n            {{\'Running\' | translate}}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-3>\n\n          <p style="\n\n            color:gray;\n\n            font-size:11px;\n\n            font-weight:400;">\n\n            <span *ngIf="item.ignOff">{{ item.ignOff }}</span>\n\n            <span *ngIf="!item.ignOff">00.00</span>&nbsp;\n\n          </p>\n\n\n\n          <p style="\n\n          font-size: 11px;\n\n          color:red; \n\n          font-weight:bold;">\n\n            {{\'Stop\' | translate}}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-3>\n\n          <p style="\n\n            color:gray;\n\n            font-size:11px;\n\n            font-weight:400;">\n\n            <span *ngIf="item.t_idling">{{ item.t_idling }}</span>\n\n            <span *ngIf="!item.t_idling">00.00</span>&nbsp;\n\n          </p>\n\n\n\n          <p style="\n\n          font-size: 11px;\n\n          color:#e6c917;\n\n          font-weight:bold;">\n\n            Idle\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-3>\n\n          <p style="\n\n            color:gray;\n\n            font-size:11px; \n\n            font-weight:400;">\n\n            <span *ngIf="item.t_ofr">{{ item.t_ofr }}</span>\n\n            <span *ngIf="!item.t_ofr">00.00</span>&nbsp;\n\n          </p>\n\n\n\n          <p style="\n\n          color:#009688;\n\n          font-size: 11px;\n\n          font-weight: bold;">\n\n            {{\'Out of reach\' | translate}}\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row style="margin-top:2%;">\n\n        <ion-col col-3>\n\n          <p style="\n\n              color:gray;\n\n              font-size:11px; \n\n              font-weight: 400;">\n\n            <span *ngIf="item.distance">{{\n\n                      item.distance | number: "1.0-2"\n\n                    }}</span>\n\n            <span *ngIf="!item.distance">00.00</span>&nbsp;\n\n          </p>\n\n\n\n          <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: bold;">\n\n            {{\'Distance\' | translate}}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-3>\n\n          <p style="\n\n              color:gray;\n\n              font-size:11px; \n\n              font-weight:400;">\n\n            <span *ngIf="item.overspeeds">{{ item.overspeeds }}{{\'Km/hr\' | translate}}</span>\n\n            <span *ngIf="!item.overspeeds">00.00</span>&nbsp;\n\n          </p>\n\n\n\n          <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: bold; margin-left: -18px;">\n\n            {{\'Overspeeding\' | translate}}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-3>\n\n          <p style="\n\n              color:gray;\n\n              font-size:11px; \n\n              font-weight: 400;">\n\n            <span *ngIf="item.routeViolations">{{ item.routeViolations }} {{ "Km/hr" | translate }}</span>\n\n            <span *ngIf="!item.routeViolations">0 {{ "Km/hr" | translate }}</span>&nbsp;\n\n          </p>\n\n\n\n          <p style="text-align:left;font-size: 11px;color:#11c1f3;  font-weight:bold; margin-left: -18px;">\n\n            {{\'Route Voilation\' | translate}}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-3>\n\n          <p style="\n\n            color:gray;\n\n            font-size:11px; \n\n            font-weight:400;">\n\n            <span>{{ item.mileage }}</span>\n\n          </p>\n\n\n\n          <p style="\n\n          color:#009688;\n\n          font-size: 11px;\n\n          font-weight: bold; ">\n\n            {{\'Fuel Con(Litre)\' | translate}}\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row style="padding-top: 5px;">\n\n        <ion-col col-1>\n\n          <ion-icon name="pin" style="color:#33c45c; font-size:15px;"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-11 (onCreate)="start_address(item, i)"  style="font-size: 0.8em;">\n\n          <div class="overme">\n\n            {{ item.StartLocation }}\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-1>\n\n          <ion-icon name="pin" style="color:#e14444;font-size:15px;"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-11 (onCreate)="end_address(item, i)"  style="font-size: 0.8em;">\n\n          <div class="overme">\n\n            {{ item.EndLocation }}\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-item>\n\n\n\n    <!-- <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/car_red_icon.png" />\n\n      </ion-avatar>\n\n      <ion-label>{{ item.Device_Name }}</ion-label>\n\n      <ion-badge item-end color="gpsc"\n\n        >{{ "Trips" | translate }} - {{ item.tripCount }}</ion-badge\n\n      >\n\n    </ion-item>\n\n    <ion-card-content>\n\n      <ion-row style="padding-top: 12px;">\n\n        <ion-col>\n\n          <p class="para">\n\n            <span *ngIf="item.ignOn">{{ item.ignOn }}</span>\n\n            <span *ngIf="!item.ignOn">00.00</span>&nbsp;\n\n          </p>\n\n          <p style="color:#53ab53;font-size:11px;font-weight: 350;">\n\n            {{ "Running" | translate }}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col>\n\n          <p class="para">\n\n            <span *ngIf="item.ignOff">{{ item.ignOff }}</span>\n\n            <span *ngIf="!item.ignOff">00.00</span>&nbsp;\n\n          </p>\n\n          <p style="text-align:left;font-size: 11px;color:red;font-weight:350;">\n\n            {{ "Stop" | translate }}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col center text-center>\n\n          <p class="para">\n\n            <span *ngIf="item.distance">{{\n\n              item.distance | number: "1.0-2"\n\n            }}</span>\n\n            <span *ngIf="!item.distance">00.00</span>&nbsp;\n\n          </p>\n\n          <p\n\n            style="text-align:left;font-size: 11px;color:#11c1f3;font-weight:350;"\n\n          >\n\n            {{ "Total" | translate }} {{ "Kms" | translate }}\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row style="padding-top: 5px;">\n\n        <ion-col>\n\n          <p class="para">\n\n            <span *ngIf="item.overspeeds">{{ item.overspeeds }}</span>\n\n            <span *ngIf="!item.overspeeds">00.00</span>&nbsp;\n\n          </p>\n\n          <p class="para1">{{ "Overspeeding" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col>\n\n          <p class="para">\n\n            <span *ngIf="item.routeViolations"\n\n              >{{ item.routeViolations }}&nbsp;Km/h</span\n\n            >\n\n            <span *ngIf="!item.routeViolations">0 {{ "Km/hr" | translate }}</span\n\n            >&nbsp;\n\n          </p>\n\n          <p class="para1">{{ "Route Voilation" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col>\n\n          <p class="para">\n\n            <span>{{ item.mileage }}</span>\n\n          </p>\n\n\n\n          <p class="para1">{{ "Fuel Con(Litre)" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row style="padding-top: 5px;">\n\n        <ion-col col-1>\n\n          <ion-icon\n\n            name="pin"\n\n            style="color:#33c45c; font-size:15px;"\n\n          ></ion-icon>\n\n        </ion-col>\n\n        <ion-col (onCreate)="start_address(item, i)" class="colSt2">\n\n          <div class="overme">\n\n            {{ item.addres }}\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-1>\n\n          <ion-icon name="pin" style="color:#e14444;font-size:15px;"></ion-icon>\n\n        </ion-col>\n\n        <ion-col (onCreate)="end_address(item, i)" class="colSt2">\n\n          <div class="overme">\n\n            {{ item.address }}\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content> -->\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\tcs2_gps\src\pages\device-summary-repo\device-summary-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], DeviceSummaryRepoPage);
    return DeviceSummaryRepoPage;
}());

//# sourceMappingURL=device-summary-repo.js.map

/***/ }),

/***/ 985:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate = /** @class */ (function () {
    function OnCreate() {
        this.onCreate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate.prototype.ngOnInit = function () {
        this.onCreate.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate.prototype, "onCreate", void 0);
    OnCreate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate);
    return OnCreate;
}());

//# sourceMappingURL=dummy-directive.js.map

/***/ })

});
//# sourceMappingURL=14.js.map