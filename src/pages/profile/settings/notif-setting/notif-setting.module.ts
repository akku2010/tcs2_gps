import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotifSettingPage } from './notif-setting';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NotifSettingPage,
  ],
  imports: [
    IonicPageModule.forChild(NotifSettingPage),
    TranslateModule.forChild()
  ],
})
export class NotifSettingPageModule {}
