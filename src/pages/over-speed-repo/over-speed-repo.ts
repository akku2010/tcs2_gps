import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-over-speed-repo',
  templateUrl: 'over-speed-repo.html',
})
export class OverSpeedRepoPage implements OnInit {


  overSpeeddevice_id: any;
  overspeedReport: any[] = [];
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any;
  isdevice: string;
  datetime: number;
  selectedVehicle: any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalloverspeed: ApiServiceProvider, public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
  }

  ngOnInit() {
    this.getdevices();
  }

  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }

  getOverspeeddevice(selectedVehicle) {
    debugger
    // console.log("selectedVehicle=> ", selectedVehicle)
    this.overSpeeddevice_id = selectedVehicle.Device_ID;
    // console.log("selected vehicle=> " + this.overSpeeddevice_id)
  }

  getdevices() {
    var baseURLp = this.apicalloverspeed.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalloverspeed.startLoading().present();
    this.apicalloverspeed.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalloverspeed.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apicalloverspeed.stopLoading();
          console.log(err);
        });
  }


  getOverSpeedReport(starttime, endtime) {
    var baseUrl;
    if (this.overSpeeddevice_id == undefined) {
      baseUrl = this.apicalloverspeed.mainUrl + "notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id

    } else {
      baseUrl = this.apicalloverspeed.mainUrl + "notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id + '&device=' + this.overSpeeddevice_id
    }
    this.apicalloverspeed.startLoading().present();
    this.apicalloverspeed.getOverSpeedApi(baseUrl)
      .subscribe(data => {
        this.apicalloverspeed.stopLoading();
        // this.overspeedReport = data;
        if (data.length > 0) {
          this.innerFunc(data);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicalloverspeed.stopLoading();
        console.log(error);
      })
  }

  innerFunc(overspeedReport) {
    let outerthis = this;
    var i = 0, howManyTimes = overspeedReport.length;
    function f() {

      outerthis.overspeedReport.push({
        'vehicleName': overspeedReport[i].vehicleName,
        'overSpeed': overspeedReport[i].overSpeed,
        'start_location': {
          'lat': overspeedReport[i].lat,
          'long': overspeedReport[i].long
        },
      });

      outerthis.start_address(outerthis.overspeedReport[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

  // start_address(item, index) {
  //   let that = this;
  //   that.overspeedReport[index].StartLocation = "N/A";
  //   if (!item.start_location) {
  //     that.overspeedReport[index].StartLocation = "N/A";
  //   } else if (item.start_location) {
  //     this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
  //       .then((res) => {
  //         console.log("test", res)
  //         var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //         that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
  //         that.overspeedReport[index].StartLocation = str;
  //       })
  //   }
  // }

  start_address(item, index) {
    let that = this;
    if (!item.start_location) {
      that.overspeedReport[index].StartLocation = "N/A";
      return;
    }
    let tempcord =  {
      "lat" : item.start_location.lat,
      "long": item.start_location.long
    }
    this.apicalloverspeed.getAddress(tempcord)
      .subscribe(res=> {
      console.log("test");
      console.log("result", res);
      console.log(res.address);
      // that.allDevices[index].address = res.address;
      if(res.message == "Address not found in databse")
      {
        this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
       .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
        that.overspeedReport[index].StartLocation = str;
        // console.log("inside", that.address);
      })
      } else {
        that.overspeedReport[index].StartLocation = res.address;
      }
    })
}


  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicalloverspeed.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

}
